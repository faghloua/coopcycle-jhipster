"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["src_main_webapp_app_entities_entity-routing_module_ts"],{

/***/ 1094:
/*!***************************************************************!*\
  !*** ./src/main/webapp/app/entities/entity-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EntityRoutingModule": () => (/* binding */ EntityRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);



class EntityRoutingModule {
}
EntityRoutingModule.ɵfac = function EntityRoutingModule_Factory(t) { return new (t || EntityRoutingModule)(); };
EntityRoutingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: EntityRoutingModule });
EntityRoutingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ imports: [[
            _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forChild([
                {
                    path: 'blog',
                    data: { pageTitle: 'blogApp.blog.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_blog_blog_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./blog/blog.module */ 7907)).then(m => m.BlogModule),
                },
                {
                    path: 'post',
                    data: { pageTitle: 'blogApp.post.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_post_post_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./post/post.module */ 5705)).then(m => m.PostModule),
                },
                {
                    path: 'tag',
                    data: { pageTitle: 'blogApp.tag.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_tag_tag_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./tag/tag.module */ 2811)).then(m => m.TagModule),
                },
                {
                    path: 'livreur',
                    data: { pageTitle: 'blogApp.livreur.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_livreur_livreur_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./livreur/livreur.module */ 4788)).then(m => m.LivreurModule),
                },
                {
                    path: 'client',
                    data: { pageTitle: 'blogApp.client.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_client_client_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./client/client.module */ 7913)).then(m => m.ClientModule),
                },
                {
                    path: 'commercant',
                    data: { pageTitle: 'blogApp.commercant.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_commercant_commercant_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./commercant/commercant.module */ 5111)).then(m => m.CommercantModule),
                },
                {
                    path: 'coperative',
                    data: { pageTitle: 'blogApp.coperative.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_coperative_coperative_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./coperative/coperative.module */ 9188)).then(m => m.CoperativeModule),
                },
                {
                    path: 'produit',
                    data: { pageTitle: 'blogApp.produit.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_produit_produit_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./produit/produit.module */ 6254)).then(m => m.ProduitModule),
                },
                {
                    path: 'commande',
                    data: { pageTitle: 'blogApp.commande.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_commande_commande_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./commande/commande.module */ 7017)).then(m => m.CommandeModule),
                },
                {
                    path: 'produit-commande',
                    data: { pageTitle: 'blogApp.produitCommande.home.title' },
                    loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_main_webapp_app_entities_produit-commande_produit-commande_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./produit-commande/produit-commande.module */ 6079)).then(m => m.ProduitCommandeModule),
                },
                /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
            ]),
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](EntityRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule] }); })();


/***/ })

}]);
//# sourceMappingURL=src_main_webapp_app_entities_entity-routing_module_ts.js.map