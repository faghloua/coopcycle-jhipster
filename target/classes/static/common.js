"use strict";
(self["webpackChunkblog"] = self["webpackChunkblog"] || []).push([["common"],{

/***/ 4218:
/*!************************************************************!*\
  !*** ./src/main/webapp/app/config/pagination.constants.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ASC": () => (/* binding */ ASC),
/* harmony export */   "DESC": () => (/* binding */ DESC),
/* harmony export */   "ITEMS_PER_PAGE": () => (/* binding */ ITEMS_PER_PAGE),
/* harmony export */   "SORT": () => (/* binding */ SORT)
/* harmony export */ });
const ITEMS_PER_PAGE = 20;
const ASC = 'asc';
const DESC = 'desc';
const SORT = 'sort';


/***/ }),

/***/ 5929:
/*!**********************************************************!*\
  !*** ./src/main/webapp/app/core/request/request-util.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createRequestOption": () => (/* binding */ createRequestOption)
/* harmony export */ });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 8784);

const createRequestOption = (req) => {
    let options = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach((val) => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};


/***/ }),

/***/ 6037:
/*!****************************************************!*\
  !*** ./src/main/webapp/app/core/util/operators.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "filterNaN": () => (/* binding */ filterNaN),
/* harmony export */   "isPresent": () => (/* binding */ isPresent)
/* harmony export */ });
/*
 * Function used to workaround https://github.com/microsoft/TypeScript/issues/16069
 * es2019 alternative `const filteredArr = myArr.flatMap((x) => x ? x : []);`
 */
function isPresent(t) {
    return t !== undefined && t !== null;
}
const filterNaN = (input) => (isNaN(input) ? 0 : input);


/***/ }),

/***/ 9827:
/*!**************************************************************!*\
  !*** ./src/main/webapp/app/core/util/parse-links.service.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ParseLinks": () => (/* binding */ ParseLinks)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);

/**
 * An utility service for link parsing.
 */
class ParseLinks {
    /**
     * Method to parse the links
     */
    parse(header) {
        if (header.length === 0) {
            throw new Error('input must not be of zero length');
        }
        // Split parts by comma
        const parts = header.split(',');
        const links = {};
        // Parse each part into a named link
        parts.forEach(p => {
            const section = p.split(';');
            if (section.length !== 2) {
                throw new Error('section could not be split on ";"');
            }
            const url = section[0].replace(/<(.*)>/, '$1').trim();
            const queryString = {};
            url.replace(/([^?=&]+)(=([^&]*))?/g, (_$0, $1, _$2, $3) => {
                if ($1 !== undefined) {
                    queryString[$1] = $3;
                }
                return $3 !== null && $3 !== void 0 ? $3 : '';
            });
            if (queryString.page !== undefined) {
                const name = section[1].replace(/rel="(.*)"/, '$1').trim();
                links[name] = parseInt(queryString.page, 10);
            }
        });
        return links;
    }
}
ParseLinks.ɵfac = function ParseLinks_Factory(t) { return new (t || ParseLinks)(); };
ParseLinks.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ParseLinks, factory: ParseLinks.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 7637:
/*!*********************************************************!*\
  !*** ./src/main/webapp/app/entities/blog/blog.model.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Blog": () => (/* binding */ Blog),
/* harmony export */   "getBlogIdentifier": () => (/* binding */ getBlogIdentifier)
/* harmony export */ });
class Blog {
    constructor(id, name, handle, user) {
        this.id = id;
        this.name = name;
        this.handle = handle;
        this.user = user;
    }
}
function getBlogIdentifier(blog) {
    return blog.id;
}


/***/ }),

/***/ 9651:
/*!*******************************************************************!*\
  !*** ./src/main/webapp/app/entities/blog/service/blog.service.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BlogService": () => (/* binding */ BlogService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _blog_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../blog.model */ 7637);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class BlogService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/blogs');
    }
    create(blog) {
        return this.http.post(this.resourceUrl, blog, { observe: 'response' });
    }
    update(blog) {
        return this.http.put(`${this.resourceUrl}/${(0,_blog_model__WEBPACK_IMPORTED_MODULE_2__.getBlogIdentifier)(blog)}`, blog, { observe: 'response' });
    }
    partialUpdate(blog) {
        return this.http.patch(`${this.resourceUrl}/${(0,_blog_model__WEBPACK_IMPORTED_MODULE_2__.getBlogIdentifier)(blog)}`, blog, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addBlogToCollectionIfMissing(blogCollection, ...blogsToCheck) {
        const blogs = blogsToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (blogs.length > 0) {
            const blogCollectionIdentifiers = blogCollection.map(blogItem => (0,_blog_model__WEBPACK_IMPORTED_MODULE_2__.getBlogIdentifier)(blogItem));
            const blogsToAdd = blogs.filter(blogItem => {
                const blogIdentifier = (0,_blog_model__WEBPACK_IMPORTED_MODULE_2__.getBlogIdentifier)(blogItem);
                if (blogIdentifier == null || blogCollectionIdentifiers.includes(blogIdentifier)) {
                    return false;
                }
                blogCollectionIdentifiers.push(blogIdentifier);
                return true;
            });
            return [...blogsToAdd, ...blogCollection];
        }
        return blogCollection;
    }
}
BlogService.ɵfac = function BlogService_Factory(t) { return new (t || BlogService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
BlogService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: BlogService, factory: BlogService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 6368:
/*!*************************************************************!*\
  !*** ./src/main/webapp/app/entities/client/client.model.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Client": () => (/* binding */ Client),
/* harmony export */   "getClientIdentifier": () => (/* binding */ getClientIdentifier)
/* harmony export */ });
class Client {
    constructor(id, idClient, idCoperative, nom, prenom, telephone, adresse, coperative, commandes) {
        this.id = id;
        this.idClient = idClient;
        this.idCoperative = idCoperative;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.adresse = adresse;
        this.coperative = coperative;
        this.commandes = commandes;
    }
}
function getClientIdentifier(client) {
    return client.id;
}


/***/ }),

/***/ 7005:
/*!***********************************************************************!*\
  !*** ./src/main/webapp/app/entities/client/service/client.service.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ClientService": () => (/* binding */ ClientService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _client_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../client.model */ 6368);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class ClientService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/clients');
    }
    create(client) {
        return this.http.post(this.resourceUrl, client, { observe: 'response' });
    }
    update(client) {
        return this.http.put(`${this.resourceUrl}/${(0,_client_model__WEBPACK_IMPORTED_MODULE_2__.getClientIdentifier)(client)}`, client, { observe: 'response' });
    }
    partialUpdate(client) {
        return this.http.patch(`${this.resourceUrl}/${(0,_client_model__WEBPACK_IMPORTED_MODULE_2__.getClientIdentifier)(client)}`, client, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addClientToCollectionIfMissing(clientCollection, ...clientsToCheck) {
        const clients = clientsToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (clients.length > 0) {
            const clientCollectionIdentifiers = clientCollection.map(clientItem => (0,_client_model__WEBPACK_IMPORTED_MODULE_2__.getClientIdentifier)(clientItem));
            const clientsToAdd = clients.filter(clientItem => {
                const clientIdentifier = (0,_client_model__WEBPACK_IMPORTED_MODULE_2__.getClientIdentifier)(clientItem);
                if (clientIdentifier == null || clientCollectionIdentifiers.includes(clientIdentifier)) {
                    return false;
                }
                clientCollectionIdentifiers.push(clientIdentifier);
                return true;
            });
            return [...clientsToAdd, ...clientCollection];
        }
        return clientCollection;
    }
}
ClientService.ɵfac = function ClientService_Factory(t) { return new (t || ClientService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
ClientService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: ClientService, factory: ClientService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2873:
/*!*********************************************************************!*\
  !*** ./src/main/webapp/app/entities/commercant/commercant.model.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Commercant": () => (/* binding */ Commercant),
/* harmony export */   "getCommercantIdentifier": () => (/* binding */ getCommercantIdentifier)
/* harmony export */ });
class Commercant {
    constructor(id, idCommercant, presentation, adresse, nom, idCoperative, disponibilite, statutCommercant, produits) {
        this.id = id;
        this.idCommercant = idCommercant;
        this.presentation = presentation;
        this.adresse = adresse;
        this.nom = nom;
        this.idCoperative = idCoperative;
        this.disponibilite = disponibilite;
        this.statutCommercant = statutCommercant;
        this.produits = produits;
    }
}
function getCommercantIdentifier(commercant) {
    return commercant.id;
}


/***/ }),

/***/ 1764:
/*!*******************************************************************************!*\
  !*** ./src/main/webapp/app/entities/commercant/service/commercant.service.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommercantService": () => (/* binding */ CommercantService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _commercant_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../commercant.model */ 2873);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class CommercantService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/commercants');
    }
    create(commercant) {
        return this.http.post(this.resourceUrl, commercant, { observe: 'response' });
    }
    update(commercant) {
        return this.http.put(`${this.resourceUrl}/${(0,_commercant_model__WEBPACK_IMPORTED_MODULE_2__.getCommercantIdentifier)(commercant)}`, commercant, {
            observe: 'response',
        });
    }
    partialUpdate(commercant) {
        return this.http.patch(`${this.resourceUrl}/${(0,_commercant_model__WEBPACK_IMPORTED_MODULE_2__.getCommercantIdentifier)(commercant)}`, commercant, {
            observe: 'response',
        });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addCommercantToCollectionIfMissing(commercantCollection, ...commercantsToCheck) {
        const commercants = commercantsToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (commercants.length > 0) {
            const commercantCollectionIdentifiers = commercantCollection.map(commercantItem => (0,_commercant_model__WEBPACK_IMPORTED_MODULE_2__.getCommercantIdentifier)(commercantItem));
            const commercantsToAdd = commercants.filter(commercantItem => {
                const commercantIdentifier = (0,_commercant_model__WEBPACK_IMPORTED_MODULE_2__.getCommercantIdentifier)(commercantItem);
                if (commercantIdentifier == null || commercantCollectionIdentifiers.includes(commercantIdentifier)) {
                    return false;
                }
                commercantCollectionIdentifiers.push(commercantIdentifier);
                return true;
            });
            return [...commercantsToAdd, ...commercantCollection];
        }
        return commercantCollection;
    }
}
CommercantService.ɵfac = function CommercantService_Factory(t) { return new (t || CommercantService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
CommercantService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: CommercantService, factory: CommercantService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 2438:
/*!*********************************************************************!*\
  !*** ./src/main/webapp/app/entities/coperative/coperative.model.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Coperative": () => (/* binding */ Coperative),
/* harmony export */   "getCoperativeIdentifier": () => (/* binding */ getCoperativeIdentifier)
/* harmony export */ });
class Coperative {
    constructor(id, idCoperative, zoneGeographique, clients, livreurs) {
        this.id = id;
        this.idCoperative = idCoperative;
        this.zoneGeographique = zoneGeographique;
        this.clients = clients;
        this.livreurs = livreurs;
    }
}
function getCoperativeIdentifier(coperative) {
    return coperative.id;
}


/***/ }),

/***/ 207:
/*!*******************************************************************************!*\
  !*** ./src/main/webapp/app/entities/coperative/service/coperative.service.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CoperativeService": () => (/* binding */ CoperativeService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _coperative_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../coperative.model */ 2438);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class CoperativeService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/coperatives');
    }
    create(coperative) {
        return this.http.post(this.resourceUrl, coperative, { observe: 'response' });
    }
    update(coperative) {
        return this.http.put(`${this.resourceUrl}/${(0,_coperative_model__WEBPACK_IMPORTED_MODULE_2__.getCoperativeIdentifier)(coperative)}`, coperative, {
            observe: 'response',
        });
    }
    partialUpdate(coperative) {
        return this.http.patch(`${this.resourceUrl}/${(0,_coperative_model__WEBPACK_IMPORTED_MODULE_2__.getCoperativeIdentifier)(coperative)}`, coperative, {
            observe: 'response',
        });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addCoperativeToCollectionIfMissing(coperativeCollection, ...coperativesToCheck) {
        const coperatives = coperativesToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (coperatives.length > 0) {
            const coperativeCollectionIdentifiers = coperativeCollection.map(coperativeItem => (0,_coperative_model__WEBPACK_IMPORTED_MODULE_2__.getCoperativeIdentifier)(coperativeItem));
            const coperativesToAdd = coperatives.filter(coperativeItem => {
                const coperativeIdentifier = (0,_coperative_model__WEBPACK_IMPORTED_MODULE_2__.getCoperativeIdentifier)(coperativeItem);
                if (coperativeIdentifier == null || coperativeCollectionIdentifiers.includes(coperativeIdentifier)) {
                    return false;
                }
                coperativeCollectionIdentifiers.push(coperativeIdentifier);
                return true;
            });
            return [...coperativesToAdd, ...coperativeCollection];
        }
        return coperativeCollection;
    }
}
CoperativeService.ɵfac = function CoperativeService_Factory(t) { return new (t || CoperativeService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
CoperativeService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: CoperativeService, factory: CoperativeService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 4188:
/*!***************************************************************!*\
  !*** ./src/main/webapp/app/entities/livreur/livreur.model.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Livreur": () => (/* binding */ Livreur),
/* harmony export */   "getLivreurIdentifier": () => (/* binding */ getLivreurIdentifier)
/* harmony export */ });
class Livreur {
    constructor(id, idLivreur, idCoperative, nom, prenom, telephone, latitude, longitude, statutLivreur, coperative, commandes) {
        this.id = id;
        this.idLivreur = idLivreur;
        this.idCoperative = idCoperative;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.latitude = latitude;
        this.longitude = longitude;
        this.statutLivreur = statutLivreur;
        this.coperative = coperative;
        this.commandes = commandes;
    }
}
function getLivreurIdentifier(livreur) {
    return livreur.id;
}


/***/ }),

/***/ 3212:
/*!*************************************************************************!*\
  !*** ./src/main/webapp/app/entities/livreur/service/livreur.service.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LivreurService": () => (/* binding */ LivreurService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _livreur_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../livreur.model */ 4188);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class LivreurService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/livreurs');
    }
    create(livreur) {
        return this.http.post(this.resourceUrl, livreur, { observe: 'response' });
    }
    update(livreur) {
        return this.http.put(`${this.resourceUrl}/${(0,_livreur_model__WEBPACK_IMPORTED_MODULE_2__.getLivreurIdentifier)(livreur)}`, livreur, { observe: 'response' });
    }
    partialUpdate(livreur) {
        return this.http.patch(`${this.resourceUrl}/${(0,_livreur_model__WEBPACK_IMPORTED_MODULE_2__.getLivreurIdentifier)(livreur)}`, livreur, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addLivreurToCollectionIfMissing(livreurCollection, ...livreursToCheck) {
        const livreurs = livreursToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (livreurs.length > 0) {
            const livreurCollectionIdentifiers = livreurCollection.map(livreurItem => (0,_livreur_model__WEBPACK_IMPORTED_MODULE_2__.getLivreurIdentifier)(livreurItem));
            const livreursToAdd = livreurs.filter(livreurItem => {
                const livreurIdentifier = (0,_livreur_model__WEBPACK_IMPORTED_MODULE_2__.getLivreurIdentifier)(livreurItem);
                if (livreurIdentifier == null || livreurCollectionIdentifiers.includes(livreurIdentifier)) {
                    return false;
                }
                livreurCollectionIdentifiers.push(livreurIdentifier);
                return true;
            });
            return [...livreursToAdd, ...livreurCollection];
        }
        return livreurCollection;
    }
}
LivreurService.ɵfac = function LivreurService_Factory(t) { return new (t || LivreurService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
LivreurService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: LivreurService, factory: LivreurService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 9571:
/*!*********************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/produit-commande.model.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProduitCommande": () => (/* binding */ ProduitCommande),
/* harmony export */   "getProduitCommandeIdentifier": () => (/* binding */ getProduitCommandeIdentifier)
/* harmony export */ });
class ProduitCommande {
    constructor(id, idProduit, idCommande, quantite, produit, commandes) {
        this.id = id;
        this.idProduit = idProduit;
        this.idCommande = idCommande;
        this.quantite = quantite;
        this.produit = produit;
        this.commandes = commandes;
    }
}
function getProduitCommandeIdentifier(produitCommande) {
    return produitCommande.id;
}


/***/ }),

/***/ 1674:
/*!*******************************************************************************************!*\
  !*** ./src/main/webapp/app/entities/produit-commande/service/produit-commande.service.ts ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProduitCommandeService": () => (/* binding */ ProduitCommandeService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _produit_commande_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../produit-commande.model */ 9571);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class ProduitCommandeService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/produit-commandes');
    }
    create(produitCommande) {
        return this.http.post(this.resourceUrl, produitCommande, { observe: 'response' });
    }
    update(produitCommande) {
        return this.http.put(`${this.resourceUrl}/${(0,_produit_commande_model__WEBPACK_IMPORTED_MODULE_2__.getProduitCommandeIdentifier)(produitCommande)}`, produitCommande, { observe: 'response' });
    }
    partialUpdate(produitCommande) {
        return this.http.patch(`${this.resourceUrl}/${(0,_produit_commande_model__WEBPACK_IMPORTED_MODULE_2__.getProduitCommandeIdentifier)(produitCommande)}`, produitCommande, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addProduitCommandeToCollectionIfMissing(produitCommandeCollection, ...produitCommandesToCheck) {
        const produitCommandes = produitCommandesToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (produitCommandes.length > 0) {
            const produitCommandeCollectionIdentifiers = produitCommandeCollection.map(produitCommandeItem => (0,_produit_commande_model__WEBPACK_IMPORTED_MODULE_2__.getProduitCommandeIdentifier)(produitCommandeItem));
            const produitCommandesToAdd = produitCommandes.filter(produitCommandeItem => {
                const produitCommandeIdentifier = (0,_produit_commande_model__WEBPACK_IMPORTED_MODULE_2__.getProduitCommandeIdentifier)(produitCommandeItem);
                if (produitCommandeIdentifier == null || produitCommandeCollectionIdentifiers.includes(produitCommandeIdentifier)) {
                    return false;
                }
                produitCommandeCollectionIdentifiers.push(produitCommandeIdentifier);
                return true;
            });
            return [...produitCommandesToAdd, ...produitCommandeCollection];
        }
        return produitCommandeCollection;
    }
}
ProduitCommandeService.ɵfac = function ProduitCommandeService_Factory(t) { return new (t || ProduitCommandeService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
ProduitCommandeService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: ProduitCommandeService, factory: ProduitCommandeService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 200:
/*!*****************************************************************!*\
  !*** ./src/main/webapp/app/entities/tag/service/tag.service.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TagService": () => (/* binding */ TagService)
/* harmony export */ });
/* harmony import */ var app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/core/util/operators */ 6037);
/* harmony import */ var app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/core/request/request-util */ 5929);
/* harmony import */ var _tag_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../tag.model */ 1904);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/core/config/application-config.service */ 1082);






class TagService {
    constructor(http, applicationConfigService) {
        this.http = http;
        this.applicationConfigService = applicationConfigService;
        this.resourceUrl = this.applicationConfigService.getEndpointFor('api/tags');
    }
    create(tag) {
        return this.http.post(this.resourceUrl, tag, { observe: 'response' });
    }
    update(tag) {
        return this.http.put(`${this.resourceUrl}/${(0,_tag_model__WEBPACK_IMPORTED_MODULE_2__.getTagIdentifier)(tag)}`, tag, { observe: 'response' });
    }
    partialUpdate(tag) {
        return this.http.patch(`${this.resourceUrl}/${(0,_tag_model__WEBPACK_IMPORTED_MODULE_2__.getTagIdentifier)(tag)}`, tag, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    query(req) {
        const options = (0,app_core_request_request_util__WEBPACK_IMPORTED_MODULE_1__.createRequestOption)(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    addTagToCollectionIfMissing(tagCollection, ...tagsToCheck) {
        const tags = tagsToCheck.filter(app_core_util_operators__WEBPACK_IMPORTED_MODULE_0__.isPresent);
        if (tags.length > 0) {
            const tagCollectionIdentifiers = tagCollection.map(tagItem => (0,_tag_model__WEBPACK_IMPORTED_MODULE_2__.getTagIdentifier)(tagItem));
            const tagsToAdd = tags.filter(tagItem => {
                const tagIdentifier = (0,_tag_model__WEBPACK_IMPORTED_MODULE_2__.getTagIdentifier)(tagItem);
                if (tagIdentifier == null || tagCollectionIdentifiers.includes(tagIdentifier)) {
                    return false;
                }
                tagCollectionIdentifiers.push(tagIdentifier);
                return true;
            });
            return [...tagsToAdd, ...tagCollection];
        }
        return tagCollection;
    }
}
TagService.ɵfac = function TagService_Factory(t) { return new (t || TagService)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__.HttpClient), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵinject"](app_core_config_application_config_service__WEBPACK_IMPORTED_MODULE_3__.ApplicationConfigService)); };
TagService.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineInjectable"]({ token: TagService, factory: TagService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 1904:
/*!*******************************************************!*\
  !*** ./src/main/webapp/app/entities/tag/tag.model.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tag": () => (/* binding */ Tag),
/* harmony export */   "getTagIdentifier": () => (/* binding */ getTagIdentifier)
/* harmony export */ });
class Tag {
    constructor(id, name, entries) {
        this.id = id;
        this.name = name;
        this.entries = entries;
    }
}
function getTagIdentifier(tag) {
    return tag.id;
}


/***/ })

}]);
//# sourceMappingURL=common.js.map