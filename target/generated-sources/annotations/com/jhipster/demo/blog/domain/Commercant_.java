package com.jhipster.demo.blog.domain;

import com.jhipster.demo.blog.domain.enumeration.StatutCommercant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Commercant.class)
public abstract class Commercant_ {

	public static volatile SingularAttribute<Commercant, String> presentation;
	public static volatile SetAttribute<Commercant, Produit> produits;
	public static volatile SingularAttribute<Commercant, Integer> idCommercant;
	public static volatile SingularAttribute<Commercant, String> disponibilite;
	public static volatile SingularAttribute<Commercant, StatutCommercant> statutCommercant;
	public static volatile SingularAttribute<Commercant, String> adresse;
	public static volatile SingularAttribute<Commercant, Integer> idCoperative;
	public static volatile SingularAttribute<Commercant, Long> id;
	public static volatile SingularAttribute<Commercant, String> nom;

	public static final String PRESENTATION = "presentation";
	public static final String PRODUITS = "produits";
	public static final String ID_COMMERCANT = "idCommercant";
	public static final String DISPONIBILITE = "disponibilite";
	public static final String STATUT_COMMERCANT = "statutCommercant";
	public static final String ADRESSE = "adresse";
	public static final String ID_COPERATIVE = "idCoperative";
	public static final String ID = "id";
	public static final String NOM = "nom";

}

