package com.jhipster.demo.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Coperative.class)
public abstract class Coperative_ {

	public static volatile SingularAttribute<Coperative, String> zoneGeographique;
	public static volatile SetAttribute<Coperative, Client> clients;
	public static volatile SetAttribute<Coperative, Livreur> livreurs;
	public static volatile SingularAttribute<Coperative, Integer> idCoperative;
	public static volatile SingularAttribute<Coperative, Long> id;

	public static final String ZONE_GEOGRAPHIQUE = "zoneGeographique";
	public static final String CLIENTS = "clients";
	public static final String LIVREURS = "livreurs";
	public static final String ID_COPERATIVE = "idCoperative";
	public static final String ID = "id";

}

