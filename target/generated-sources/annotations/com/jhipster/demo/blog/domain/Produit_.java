package com.jhipster.demo.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Produit.class)
public abstract class Produit_ {

	public static volatile SingularAttribute<Produit, Integer> idProduit;
	public static volatile SingularAttribute<Produit, Float> prix;
	public static volatile SingularAttribute<Produit, Commercant> commercant;
	public static volatile SingularAttribute<Produit, String> description;
	public static volatile SingularAttribute<Produit, Integer> dispinobilite;
	public static volatile SingularAttribute<Produit, Long> id;
	public static volatile SingularAttribute<Produit, String> nomProduit;
	public static volatile SingularAttribute<Produit, ProduitCommande> produitCommande;

	public static final String ID_PRODUIT = "idProduit";
	public static final String PRIX = "prix";
	public static final String COMMERCANT = "commercant";
	public static final String DESCRIPTION = "description";
	public static final String DISPINOBILITE = "dispinobilite";
	public static final String ID = "id";
	public static final String NOM_PRODUIT = "nomProduit";
	public static final String PRODUIT_COMMANDE = "produitCommande";

}

