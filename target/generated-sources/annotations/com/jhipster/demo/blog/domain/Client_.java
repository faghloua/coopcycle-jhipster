package com.jhipster.demo.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Client.class)
public abstract class Client_ {

	public static volatile SetAttribute<Client, Commande> commandes;
	public static volatile SingularAttribute<Client, Integer> idClient;
	public static volatile SingularAttribute<Client, Coperative> coperative;
	public static volatile SingularAttribute<Client, String> adresse;
	public static volatile SingularAttribute<Client, Integer> idCoperative;
	public static volatile SingularAttribute<Client, String> telephone;
	public static volatile SingularAttribute<Client, Long> id;
	public static volatile SingularAttribute<Client, String> nom;
	public static volatile SingularAttribute<Client, String> prenom;

	public static final String COMMANDES = "commandes";
	public static final String ID_CLIENT = "idClient";
	public static final String COPERATIVE = "coperative";
	public static final String ADRESSE = "adresse";
	public static final String ID_COPERATIVE = "idCoperative";
	public static final String TELEPHONE = "telephone";
	public static final String ID = "id";
	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";

}

