package com.jhipster.demo.blog.domain;

import com.jhipster.demo.blog.domain.enumeration.StatutCommande;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Commande.class)
public abstract class Commande_ {

	public static volatile SetAttribute<Commande, ProduitCommande> produitCommandes;
	public static volatile SingularAttribute<Commande, Integer> idClient;
	public static volatile SingularAttribute<Commande, Integer> idCoperative;
	public static volatile SingularAttribute<Commande, StatutCommande> statutCommande;
	public static volatile SingularAttribute<Commande, Client> client;
	public static volatile SingularAttribute<Commande, Long> id;
	public static volatile SingularAttribute<Commande, Livreur> livreur;
	public static volatile SingularAttribute<Commande, Integer> idCommande;
	public static volatile SingularAttribute<Commande, Integer> idLivreur;

	public static final String PRODUIT_COMMANDES = "produitCommandes";
	public static final String ID_CLIENT = "idClient";
	public static final String ID_COPERATIVE = "idCoperative";
	public static final String STATUT_COMMANDE = "statutCommande";
	public static final String CLIENT = "client";
	public static final String ID = "id";
	public static final String LIVREUR = "livreur";
	public static final String ID_COMMANDE = "idCommande";
	public static final String ID_LIVREUR = "idLivreur";

}

