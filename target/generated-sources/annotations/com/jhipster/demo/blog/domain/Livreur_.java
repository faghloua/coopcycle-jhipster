package com.jhipster.demo.blog.domain;

import com.jhipster.demo.blog.domain.enumeration.StatutLivreur;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Livreur.class)
public abstract class Livreur_ {

	public static volatile SetAttribute<Livreur, Commande> commandes;
	public static volatile SingularAttribute<Livreur, StatutLivreur> statutLivreur;
	public static volatile SingularAttribute<Livreur, Coperative> coperative;
	public static volatile SingularAttribute<Livreur, Float> latitude;
	public static volatile SingularAttribute<Livreur, Integer> idCoperative;
	public static volatile SingularAttribute<Livreur, String> telephone;
	public static volatile SingularAttribute<Livreur, Long> id;
	public static volatile SingularAttribute<Livreur, String> nom;
	public static volatile SingularAttribute<Livreur, String> prenom;
	public static volatile SingularAttribute<Livreur, Integer> idLivreur;
	public static volatile SingularAttribute<Livreur, Float> longitude;

	public static final String COMMANDES = "commandes";
	public static final String STATUT_LIVREUR = "statutLivreur";
	public static final String COPERATIVE = "coperative";
	public static final String LATITUDE = "latitude";
	public static final String ID_COPERATIVE = "idCoperative";
	public static final String TELEPHONE = "telephone";
	public static final String ID = "id";
	public static final String NOM = "nom";
	public static final String PRENOM = "prenom";
	public static final String ID_LIVREUR = "idLivreur";
	public static final String LONGITUDE = "longitude";

}

