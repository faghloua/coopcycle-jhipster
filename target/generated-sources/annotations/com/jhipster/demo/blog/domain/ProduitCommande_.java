package com.jhipster.demo.blog.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProduitCommande.class)
public abstract class ProduitCommande_ {

	public static volatile SetAttribute<ProduitCommande, Commande> commandes;
	public static volatile SingularAttribute<ProduitCommande, Integer> idProduit;
	public static volatile SingularAttribute<ProduitCommande, Produit> produit;
	public static volatile SingularAttribute<ProduitCommande, Long> id;
	public static volatile SingularAttribute<ProduitCommande, Integer> idCommande;
	public static volatile SingularAttribute<ProduitCommande, Integer> quantite;

	public static final String COMMANDES = "commandes";
	public static final String ID_PRODUIT = "idProduit";
	public static final String PRODUIT = "produit";
	public static final String ID = "id";
	public static final String ID_COMMANDE = "idCommande";
	public static final String QUANTITE = "quantite";

}

