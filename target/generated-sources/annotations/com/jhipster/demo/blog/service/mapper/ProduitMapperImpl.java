package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Commercant;
import com.jhipster.demo.blog.domain.Produit;
import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.service.dto.CommercantDTO;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import com.jhipster.demo.blog.service.dto.ProduitDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:17+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class ProduitMapperImpl implements ProduitMapper {

    @Override
    public Produit toEntity(ProduitDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Produit produit = new Produit();

        produit.setId( dto.getId() );
        produit.setIdProduit( dto.getIdProduit() );
        produit.setNomProduit( dto.getNomProduit() );
        produit.setPrix( dto.getPrix() );
        produit.setDescription( dto.getDescription() );
        produit.setDispinobilite( dto.getDispinobilite() );
        produit.produitCommande( produitCommandeDTOToProduitCommande( dto.getProduitCommande() ) );
        produit.commercant( commercantDTOToCommercant( dto.getCommercant() ) );

        return produit;
    }

    @Override
    public List<Produit> toEntity(List<ProduitDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Produit> list = new ArrayList<Produit>( dtoList.size() );
        for ( ProduitDTO produitDTO : dtoList ) {
            list.add( toEntity( produitDTO ) );
        }

        return list;
    }

    @Override
    public List<ProduitDTO> toDto(List<Produit> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ProduitDTO> list = new ArrayList<ProduitDTO>( entityList.size() );
        for ( Produit produit : entityList ) {
            list.add( toDto( produit ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Produit entity, ProduitDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdProduit() != null ) {
            entity.setIdProduit( dto.getIdProduit() );
        }
        if ( dto.getNomProduit() != null ) {
            entity.setNomProduit( dto.getNomProduit() );
        }
        if ( dto.getPrix() != null ) {
            entity.setPrix( dto.getPrix() );
        }
        if ( dto.getDescription() != null ) {
            entity.setDescription( dto.getDescription() );
        }
        if ( dto.getDispinobilite() != null ) {
            entity.setDispinobilite( dto.getDispinobilite() );
        }
        if ( dto.getProduitCommande() != null ) {
            if ( entity.getProduitCommande() == null ) {
                entity.produitCommande( new ProduitCommande() );
            }
            produitCommandeDTOToProduitCommande1( dto.getProduitCommande(), entity.getProduitCommande() );
        }
        if ( dto.getCommercant() != null ) {
            if ( entity.getCommercant() == null ) {
                entity.commercant( new Commercant() );
            }
            commercantDTOToCommercant1( dto.getCommercant(), entity.getCommercant() );
        }
    }

    @Override
    public ProduitDTO toDto(Produit s) {
        if ( s == null ) {
            return null;
        }

        ProduitDTO produitDTO = new ProduitDTO();

        produitDTO.setProduitCommande( toDtoProduitCommandeId( s.getProduitCommande() ) );
        produitDTO.setCommercant( toDtoCommercantId( s.getCommercant() ) );
        produitDTO.setId( s.getId() );
        produitDTO.setIdProduit( s.getIdProduit() );
        produitDTO.setNomProduit( s.getNomProduit() );
        produitDTO.setPrix( s.getPrix() );
        produitDTO.setDescription( s.getDescription() );
        produitDTO.setDispinobilite( s.getDispinobilite() );

        return produitDTO;
    }

    @Override
    public ProduitCommandeDTO toDtoProduitCommandeId(ProduitCommande produitCommande) {
        if ( produitCommande == null ) {
            return null;
        }

        ProduitCommandeDTO produitCommandeDTO = new ProduitCommandeDTO();

        produitCommandeDTO.setId( produitCommande.getId() );

        return produitCommandeDTO;
    }

    @Override
    public CommercantDTO toDtoCommercantId(Commercant commercant) {
        if ( commercant == null ) {
            return null;
        }

        CommercantDTO commercantDTO = new CommercantDTO();

        commercantDTO.setId( commercant.getId() );

        return commercantDTO;
    }

    protected ProduitCommande produitCommandeDTOToProduitCommande(ProduitCommandeDTO produitCommandeDTO) {
        if ( produitCommandeDTO == null ) {
            return null;
        }

        ProduitCommande produitCommande = new ProduitCommande();

        produitCommande.setId( produitCommandeDTO.getId() );
        produitCommande.setIdProduit( produitCommandeDTO.getIdProduit() );
        produitCommande.setIdCommande( produitCommandeDTO.getIdCommande() );
        produitCommande.setQuantite( produitCommandeDTO.getQuantite() );

        return produitCommande;
    }

    protected Commercant commercantDTOToCommercant(CommercantDTO commercantDTO) {
        if ( commercantDTO == null ) {
            return null;
        }

        Commercant commercant = new Commercant();

        commercant.setId( commercantDTO.getId() );
        commercant.setIdCommercant( commercantDTO.getIdCommercant() );
        commercant.setPresentation( commercantDTO.getPresentation() );
        commercant.setAdresse( commercantDTO.getAdresse() );
        commercant.setNom( commercantDTO.getNom() );
        commercant.setIdCoperative( commercantDTO.getIdCoperative() );
        commercant.setDisponibilite( commercantDTO.getDisponibilite() );
        commercant.setStatutCommercant( commercantDTO.getStatutCommercant() );

        return commercant;
    }

    protected void produitCommandeDTOToProduitCommande1(ProduitCommandeDTO produitCommandeDTO, ProduitCommande mappingTarget) {
        if ( produitCommandeDTO == null ) {
            return;
        }

        if ( produitCommandeDTO.getId() != null ) {
            mappingTarget.setId( produitCommandeDTO.getId() );
        }
        if ( produitCommandeDTO.getIdProduit() != null ) {
            mappingTarget.setIdProduit( produitCommandeDTO.getIdProduit() );
        }
        if ( produitCommandeDTO.getIdCommande() != null ) {
            mappingTarget.setIdCommande( produitCommandeDTO.getIdCommande() );
        }
        if ( produitCommandeDTO.getQuantite() != null ) {
            mappingTarget.setQuantite( produitCommandeDTO.getQuantite() );
        }
    }

    protected void commercantDTOToCommercant1(CommercantDTO commercantDTO, Commercant mappingTarget) {
        if ( commercantDTO == null ) {
            return;
        }

        if ( commercantDTO.getId() != null ) {
            mappingTarget.setId( commercantDTO.getId() );
        }
        if ( commercantDTO.getIdCommercant() != null ) {
            mappingTarget.setIdCommercant( commercantDTO.getIdCommercant() );
        }
        if ( commercantDTO.getPresentation() != null ) {
            mappingTarget.setPresentation( commercantDTO.getPresentation() );
        }
        if ( commercantDTO.getAdresse() != null ) {
            mappingTarget.setAdresse( commercantDTO.getAdresse() );
        }
        if ( commercantDTO.getNom() != null ) {
            mappingTarget.setNom( commercantDTO.getNom() );
        }
        if ( commercantDTO.getIdCoperative() != null ) {
            mappingTarget.setIdCoperative( commercantDTO.getIdCoperative() );
        }
        if ( commercantDTO.getDisponibilite() != null ) {
            mappingTarget.setDisponibilite( commercantDTO.getDisponibilite() );
        }
        if ( commercantDTO.getStatutCommercant() != null ) {
            mappingTarget.setStatutCommercant( commercantDTO.getStatutCommercant() );
        }
    }
}
