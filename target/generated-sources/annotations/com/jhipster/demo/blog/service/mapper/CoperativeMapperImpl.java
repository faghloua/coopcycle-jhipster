package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:17+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class CoperativeMapperImpl implements CoperativeMapper {

    @Override
    public Coperative toEntity(CoperativeDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Coperative coperative = new Coperative();

        coperative.setId( dto.getId() );
        coperative.setIdCoperative( dto.getIdCoperative() );
        coperative.setZoneGeographique( dto.getZoneGeographique() );

        return coperative;
    }

    @Override
    public CoperativeDTO toDto(Coperative entity) {
        if ( entity == null ) {
            return null;
        }

        CoperativeDTO coperativeDTO = new CoperativeDTO();

        coperativeDTO.setId( entity.getId() );
        coperativeDTO.setIdCoperative( entity.getIdCoperative() );
        coperativeDTO.setZoneGeographique( entity.getZoneGeographique() );

        return coperativeDTO;
    }

    @Override
    public List<Coperative> toEntity(List<CoperativeDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Coperative> list = new ArrayList<Coperative>( dtoList.size() );
        for ( CoperativeDTO coperativeDTO : dtoList ) {
            list.add( toEntity( coperativeDTO ) );
        }

        return list;
    }

    @Override
    public List<CoperativeDTO> toDto(List<Coperative> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CoperativeDTO> list = new ArrayList<CoperativeDTO>( entityList.size() );
        for ( Coperative coperative : entityList ) {
            list.add( toDto( coperative ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Coperative entity, CoperativeDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdCoperative() != null ) {
            entity.setIdCoperative( dto.getIdCoperative() );
        }
        if ( dto.getZoneGeographique() != null ) {
            entity.setZoneGeographique( dto.getZoneGeographique() );
        }
    }
}
