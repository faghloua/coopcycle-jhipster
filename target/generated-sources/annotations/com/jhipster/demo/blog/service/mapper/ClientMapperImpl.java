package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Client;
import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.service.dto.ClientDTO;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:18+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class ClientMapperImpl implements ClientMapper {

    @Override
    public Client toEntity(ClientDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Client client = new Client();

        client.setId( dto.getId() );
        client.setIdClient( dto.getIdClient() );
        client.setIdCoperative( dto.getIdCoperative() );
        client.setNom( dto.getNom() );
        client.setPrenom( dto.getPrenom() );
        client.setTelephone( dto.getTelephone() );
        client.setAdresse( dto.getAdresse() );
        client.coperative( coperativeDTOToCoperative( dto.getCoperative() ) );

        return client;
    }

    @Override
    public List<Client> toEntity(List<ClientDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Client> list = new ArrayList<Client>( dtoList.size() );
        for ( ClientDTO clientDTO : dtoList ) {
            list.add( toEntity( clientDTO ) );
        }

        return list;
    }

    @Override
    public List<ClientDTO> toDto(List<Client> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ClientDTO> list = new ArrayList<ClientDTO>( entityList.size() );
        for ( Client client : entityList ) {
            list.add( toDto( client ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Client entity, ClientDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdClient() != null ) {
            entity.setIdClient( dto.getIdClient() );
        }
        if ( dto.getIdCoperative() != null ) {
            entity.setIdCoperative( dto.getIdCoperative() );
        }
        if ( dto.getNom() != null ) {
            entity.setNom( dto.getNom() );
        }
        if ( dto.getPrenom() != null ) {
            entity.setPrenom( dto.getPrenom() );
        }
        if ( dto.getTelephone() != null ) {
            entity.setTelephone( dto.getTelephone() );
        }
        if ( dto.getAdresse() != null ) {
            entity.setAdresse( dto.getAdresse() );
        }
        if ( dto.getCoperative() != null ) {
            if ( entity.getCoperative() == null ) {
                entity.coperative( new Coperative() );
            }
            coperativeDTOToCoperative1( dto.getCoperative(), entity.getCoperative() );
        }
    }

    @Override
    public ClientDTO toDto(Client s) {
        if ( s == null ) {
            return null;
        }

        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setCoperative( toDtoCoperativeId( s.getCoperative() ) );
        clientDTO.setId( s.getId() );
        clientDTO.setIdClient( s.getIdClient() );
        clientDTO.setIdCoperative( s.getIdCoperative() );
        clientDTO.setNom( s.getNom() );
        clientDTO.setPrenom( s.getPrenom() );
        clientDTO.setTelephone( s.getTelephone() );
        clientDTO.setAdresse( s.getAdresse() );

        return clientDTO;
    }

    @Override
    public CoperativeDTO toDtoCoperativeId(Coperative coperative) {
        if ( coperative == null ) {
            return null;
        }

        CoperativeDTO coperativeDTO = new CoperativeDTO();

        coperativeDTO.setId( coperative.getId() );

        return coperativeDTO;
    }

    protected Coperative coperativeDTOToCoperative(CoperativeDTO coperativeDTO) {
        if ( coperativeDTO == null ) {
            return null;
        }

        Coperative coperative = new Coperative();

        coperative.setId( coperativeDTO.getId() );
        coperative.setIdCoperative( coperativeDTO.getIdCoperative() );
        coperative.setZoneGeographique( coperativeDTO.getZoneGeographique() );

        return coperative;
    }

    protected void coperativeDTOToCoperative1(CoperativeDTO coperativeDTO, Coperative mappingTarget) {
        if ( coperativeDTO == null ) {
            return;
        }

        if ( coperativeDTO.getId() != null ) {
            mappingTarget.setId( coperativeDTO.getId() );
        }
        if ( coperativeDTO.getIdCoperative() != null ) {
            mappingTarget.setIdCoperative( coperativeDTO.getIdCoperative() );
        }
        if ( coperativeDTO.getZoneGeographique() != null ) {
            mappingTarget.setZoneGeographique( coperativeDTO.getZoneGeographique() );
        }
    }
}
