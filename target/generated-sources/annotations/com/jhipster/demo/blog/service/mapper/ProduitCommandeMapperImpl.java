package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:18+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class ProduitCommandeMapperImpl implements ProduitCommandeMapper {

    @Override
    public ProduitCommande toEntity(ProduitCommandeDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ProduitCommande produitCommande = new ProduitCommande();

        produitCommande.setId( dto.getId() );
        produitCommande.setIdProduit( dto.getIdProduit() );
        produitCommande.setIdCommande( dto.getIdCommande() );
        produitCommande.setQuantite( dto.getQuantite() );

        return produitCommande;
    }

    @Override
    public ProduitCommandeDTO toDto(ProduitCommande entity) {
        if ( entity == null ) {
            return null;
        }

        ProduitCommandeDTO produitCommandeDTO = new ProduitCommandeDTO();

        produitCommandeDTO.setId( entity.getId() );
        produitCommandeDTO.setIdProduit( entity.getIdProduit() );
        produitCommandeDTO.setIdCommande( entity.getIdCommande() );
        produitCommandeDTO.setQuantite( entity.getQuantite() );

        return produitCommandeDTO;
    }

    @Override
    public List<ProduitCommande> toEntity(List<ProduitCommandeDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ProduitCommande> list = new ArrayList<ProduitCommande>( dtoList.size() );
        for ( ProduitCommandeDTO produitCommandeDTO : dtoList ) {
            list.add( toEntity( produitCommandeDTO ) );
        }

        return list;
    }

    @Override
    public List<ProduitCommandeDTO> toDto(List<ProduitCommande> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ProduitCommandeDTO> list = new ArrayList<ProduitCommandeDTO>( entityList.size() );
        for ( ProduitCommande produitCommande : entityList ) {
            list.add( toDto( produitCommande ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(ProduitCommande entity, ProduitCommandeDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdProduit() != null ) {
            entity.setIdProduit( dto.getIdProduit() );
        }
        if ( dto.getIdCommande() != null ) {
            entity.setIdCommande( dto.getIdCommande() );
        }
        if ( dto.getQuantite() != null ) {
            entity.setQuantite( dto.getQuantite() );
        }
    }
}
