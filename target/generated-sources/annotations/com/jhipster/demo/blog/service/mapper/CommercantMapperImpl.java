package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Commercant;
import com.jhipster.demo.blog.service.dto.CommercantDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:18+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class CommercantMapperImpl implements CommercantMapper {

    @Override
    public Commercant toEntity(CommercantDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Commercant commercant = new Commercant();

        commercant.setId( dto.getId() );
        commercant.setIdCommercant( dto.getIdCommercant() );
        commercant.setPresentation( dto.getPresentation() );
        commercant.setAdresse( dto.getAdresse() );
        commercant.setNom( dto.getNom() );
        commercant.setIdCoperative( dto.getIdCoperative() );
        commercant.setDisponibilite( dto.getDisponibilite() );
        commercant.setStatutCommercant( dto.getStatutCommercant() );

        return commercant;
    }

    @Override
    public CommercantDTO toDto(Commercant entity) {
        if ( entity == null ) {
            return null;
        }

        CommercantDTO commercantDTO = new CommercantDTO();

        commercantDTO.setId( entity.getId() );
        commercantDTO.setIdCommercant( entity.getIdCommercant() );
        commercantDTO.setPresentation( entity.getPresentation() );
        commercantDTO.setAdresse( entity.getAdresse() );
        commercantDTO.setNom( entity.getNom() );
        commercantDTO.setIdCoperative( entity.getIdCoperative() );
        commercantDTO.setDisponibilite( entity.getDisponibilite() );
        commercantDTO.setStatutCommercant( entity.getStatutCommercant() );

        return commercantDTO;
    }

    @Override
    public List<Commercant> toEntity(List<CommercantDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Commercant> list = new ArrayList<Commercant>( dtoList.size() );
        for ( CommercantDTO commercantDTO : dtoList ) {
            list.add( toEntity( commercantDTO ) );
        }

        return list;
    }

    @Override
    public List<CommercantDTO> toDto(List<Commercant> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CommercantDTO> list = new ArrayList<CommercantDTO>( entityList.size() );
        for ( Commercant commercant : entityList ) {
            list.add( toDto( commercant ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Commercant entity, CommercantDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdCommercant() != null ) {
            entity.setIdCommercant( dto.getIdCommercant() );
        }
        if ( dto.getPresentation() != null ) {
            entity.setPresentation( dto.getPresentation() );
        }
        if ( dto.getAdresse() != null ) {
            entity.setAdresse( dto.getAdresse() );
        }
        if ( dto.getNom() != null ) {
            entity.setNom( dto.getNom() );
        }
        if ( dto.getIdCoperative() != null ) {
            entity.setIdCoperative( dto.getIdCoperative() );
        }
        if ( dto.getDisponibilite() != null ) {
            entity.setDisponibilite( dto.getDisponibilite() );
        }
        if ( dto.getStatutCommercant() != null ) {
            entity.setStatutCommercant( dto.getStatutCommercant() );
        }
    }
}
