package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Client;
import com.jhipster.demo.blog.domain.Commande;
import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.domain.Livreur;
import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.service.dto.ClientDTO;
import com.jhipster.demo.blog.service.dto.CommandeDTO;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import com.jhipster.demo.blog.service.dto.LivreurDTO;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:17+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class CommandeMapperImpl implements CommandeMapper {

    @Override
    public List<Commande> toEntity(List<CommandeDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Commande> list = new ArrayList<Commande>( dtoList.size() );
        for ( CommandeDTO commandeDTO : dtoList ) {
            list.add( toEntity( commandeDTO ) );
        }

        return list;
    }

    @Override
    public List<CommandeDTO> toDto(List<Commande> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CommandeDTO> list = new ArrayList<CommandeDTO>( entityList.size() );
        for ( Commande commande : entityList ) {
            list.add( toDto( commande ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Commande entity, CommandeDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdCommande() != null ) {
            entity.setIdCommande( dto.getIdCommande() );
        }
        if ( dto.getIdClient() != null ) {
            entity.setIdClient( dto.getIdClient() );
        }
        if ( dto.getIdLivreur() != null ) {
            entity.setIdLivreur( dto.getIdLivreur() );
        }
        if ( dto.getIdCoperative() != null ) {
            entity.setIdCoperative( dto.getIdCoperative() );
        }
        if ( dto.getStatutCommande() != null ) {
            entity.setStatutCommande( dto.getStatutCommande() );
        }
        if ( dto.getClient() != null ) {
            if ( entity.getClient() == null ) {
                entity.client( new Client() );
            }
            clientDTOToClient( dto.getClient(), entity.getClient() );
        }
        if ( dto.getLivreur() != null ) {
            if ( entity.getLivreur() == null ) {
                entity.livreur( new Livreur() );
            }
            livreurDTOToLivreur( dto.getLivreur(), entity.getLivreur() );
        }
        if ( entity.getProduitCommandes() != null ) {
            Set<ProduitCommande> set = produitCommandeDTOSetToProduitCommandeSet( dto.getProduitCommandes() );
            if ( set != null ) {
                entity.getProduitCommandes().clear();
                entity.getProduitCommandes().addAll( set );
            }
        }
        else {
            Set<ProduitCommande> set = produitCommandeDTOSetToProduitCommandeSet( dto.getProduitCommandes() );
            if ( set != null ) {
                entity.produitCommandes( set );
            }
        }
    }

    @Override
    public CommandeDTO toDto(Commande s) {
        if ( s == null ) {
            return null;
        }

        CommandeDTO commandeDTO = new CommandeDTO();

        commandeDTO.setClient( toDtoClientId( s.getClient() ) );
        commandeDTO.setLivreur( toDtoLivreurId( s.getLivreur() ) );
        commandeDTO.setProduitCommandes( toDtoProduitCommandeIdSet( s.getProduitCommandes() ) );
        commandeDTO.setId( s.getId() );
        commandeDTO.setIdCommande( s.getIdCommande() );
        commandeDTO.setIdClient( s.getIdClient() );
        commandeDTO.setIdLivreur( s.getIdLivreur() );
        commandeDTO.setIdCoperative( s.getIdCoperative() );
        commandeDTO.setStatutCommande( s.getStatutCommande() );

        return commandeDTO;
    }

    @Override
    public Commande toEntity(CommandeDTO commandeDTO) {
        if ( commandeDTO == null ) {
            return null;
        }

        Commande commande = new Commande();

        commande.setId( commandeDTO.getId() );
        commande.setIdCommande( commandeDTO.getIdCommande() );
        commande.setIdClient( commandeDTO.getIdClient() );
        commande.setIdLivreur( commandeDTO.getIdLivreur() );
        commande.setIdCoperative( commandeDTO.getIdCoperative() );
        commande.setStatutCommande( commandeDTO.getStatutCommande() );
        commande.client( clientDTOToClient1( commandeDTO.getClient() ) );
        commande.livreur( livreurDTOToLivreur1( commandeDTO.getLivreur() ) );
        commande.produitCommandes( produitCommandeDTOSetToProduitCommandeSet( commandeDTO.getProduitCommandes() ) );

        return commande;
    }

    @Override
    public ClientDTO toDtoClientId(Client client) {
        if ( client == null ) {
            return null;
        }

        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setId( client.getId() );

        return clientDTO;
    }

    @Override
    public LivreurDTO toDtoLivreurId(Livreur livreur) {
        if ( livreur == null ) {
            return null;
        }

        LivreurDTO livreurDTO = new LivreurDTO();

        livreurDTO.setId( livreur.getId() );

        return livreurDTO;
    }

    @Override
    public ProduitCommandeDTO toDtoProduitCommandeId(ProduitCommande produitCommande) {
        if ( produitCommande == null ) {
            return null;
        }

        ProduitCommandeDTO produitCommandeDTO = new ProduitCommandeDTO();

        produitCommandeDTO.setId( produitCommande.getId() );

        return produitCommandeDTO;
    }

    protected void coperativeDTOToCoperative(CoperativeDTO coperativeDTO, Coperative mappingTarget) {
        if ( coperativeDTO == null ) {
            return;
        }

        if ( coperativeDTO.getId() != null ) {
            mappingTarget.setId( coperativeDTO.getId() );
        }
        if ( coperativeDTO.getIdCoperative() != null ) {
            mappingTarget.setIdCoperative( coperativeDTO.getIdCoperative() );
        }
        if ( coperativeDTO.getZoneGeographique() != null ) {
            mappingTarget.setZoneGeographique( coperativeDTO.getZoneGeographique() );
        }
    }

    protected void clientDTOToClient(ClientDTO clientDTO, Client mappingTarget) {
        if ( clientDTO == null ) {
            return;
        }

        if ( clientDTO.getId() != null ) {
            mappingTarget.setId( clientDTO.getId() );
        }
        if ( clientDTO.getIdClient() != null ) {
            mappingTarget.setIdClient( clientDTO.getIdClient() );
        }
        if ( clientDTO.getIdCoperative() != null ) {
            mappingTarget.setIdCoperative( clientDTO.getIdCoperative() );
        }
        if ( clientDTO.getNom() != null ) {
            mappingTarget.setNom( clientDTO.getNom() );
        }
        if ( clientDTO.getPrenom() != null ) {
            mappingTarget.setPrenom( clientDTO.getPrenom() );
        }
        if ( clientDTO.getTelephone() != null ) {
            mappingTarget.setTelephone( clientDTO.getTelephone() );
        }
        if ( clientDTO.getAdresse() != null ) {
            mappingTarget.setAdresse( clientDTO.getAdresse() );
        }
        if ( clientDTO.getCoperative() != null ) {
            if ( mappingTarget.getCoperative() == null ) {
                mappingTarget.coperative( new Coperative() );
            }
            coperativeDTOToCoperative( clientDTO.getCoperative(), mappingTarget.getCoperative() );
        }
    }

    protected void livreurDTOToLivreur(LivreurDTO livreurDTO, Livreur mappingTarget) {
        if ( livreurDTO == null ) {
            return;
        }

        if ( livreurDTO.getId() != null ) {
            mappingTarget.setId( livreurDTO.getId() );
        }
        if ( livreurDTO.getIdLivreur() != null ) {
            mappingTarget.setIdLivreur( livreurDTO.getIdLivreur() );
        }
        if ( livreurDTO.getIdCoperative() != null ) {
            mappingTarget.setIdCoperative( livreurDTO.getIdCoperative() );
        }
        if ( livreurDTO.getNom() != null ) {
            mappingTarget.setNom( livreurDTO.getNom() );
        }
        if ( livreurDTO.getPrenom() != null ) {
            mappingTarget.setPrenom( livreurDTO.getPrenom() );
        }
        if ( livreurDTO.getTelephone() != null ) {
            mappingTarget.setTelephone( livreurDTO.getTelephone() );
        }
        if ( livreurDTO.getLatitude() != null ) {
            mappingTarget.setLatitude( livreurDTO.getLatitude() );
        }
        if ( livreurDTO.getLongitude() != null ) {
            mappingTarget.setLongitude( livreurDTO.getLongitude() );
        }
        if ( livreurDTO.getStatutLivreur() != null ) {
            mappingTarget.setStatutLivreur( livreurDTO.getStatutLivreur() );
        }
        if ( livreurDTO.getCoperative() != null ) {
            if ( mappingTarget.getCoperative() == null ) {
                mappingTarget.coperative( new Coperative() );
            }
            coperativeDTOToCoperative( livreurDTO.getCoperative(), mappingTarget.getCoperative() );
        }
    }

    protected ProduitCommande produitCommandeDTOToProduitCommande(ProduitCommandeDTO produitCommandeDTO) {
        if ( produitCommandeDTO == null ) {
            return null;
        }

        ProduitCommande produitCommande = new ProduitCommande();

        produitCommande.setId( produitCommandeDTO.getId() );
        produitCommande.setIdProduit( produitCommandeDTO.getIdProduit() );
        produitCommande.setIdCommande( produitCommandeDTO.getIdCommande() );
        produitCommande.setQuantite( produitCommandeDTO.getQuantite() );

        return produitCommande;
    }

    protected Set<ProduitCommande> produitCommandeDTOSetToProduitCommandeSet(Set<ProduitCommandeDTO> set) {
        if ( set == null ) {
            return null;
        }

        Set<ProduitCommande> set1 = new HashSet<ProduitCommande>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( ProduitCommandeDTO produitCommandeDTO : set ) {
            set1.add( produitCommandeDTOToProduitCommande( produitCommandeDTO ) );
        }

        return set1;
    }

    protected Coperative coperativeDTOToCoperative1(CoperativeDTO coperativeDTO) {
        if ( coperativeDTO == null ) {
            return null;
        }

        Coperative coperative = new Coperative();

        coperative.setId( coperativeDTO.getId() );
        coperative.setIdCoperative( coperativeDTO.getIdCoperative() );
        coperative.setZoneGeographique( coperativeDTO.getZoneGeographique() );

        return coperative;
    }

    protected Client clientDTOToClient1(ClientDTO clientDTO) {
        if ( clientDTO == null ) {
            return null;
        }

        Client client = new Client();

        client.setId( clientDTO.getId() );
        client.setIdClient( clientDTO.getIdClient() );
        client.setIdCoperative( clientDTO.getIdCoperative() );
        client.setNom( clientDTO.getNom() );
        client.setPrenom( clientDTO.getPrenom() );
        client.setTelephone( clientDTO.getTelephone() );
        client.setAdresse( clientDTO.getAdresse() );
        client.coperative( coperativeDTOToCoperative1( clientDTO.getCoperative() ) );

        return client;
    }

    protected Livreur livreurDTOToLivreur1(LivreurDTO livreurDTO) {
        if ( livreurDTO == null ) {
            return null;
        }

        Livreur livreur = new Livreur();

        livreur.setId( livreurDTO.getId() );
        livreur.setIdLivreur( livreurDTO.getIdLivreur() );
        livreur.setIdCoperative( livreurDTO.getIdCoperative() );
        livreur.setNom( livreurDTO.getNom() );
        livreur.setPrenom( livreurDTO.getPrenom() );
        livreur.setTelephone( livreurDTO.getTelephone() );
        livreur.setLatitude( livreurDTO.getLatitude() );
        livreur.setLongitude( livreurDTO.getLongitude() );
        livreur.setStatutLivreur( livreurDTO.getStatutLivreur() );
        livreur.coperative( coperativeDTOToCoperative1( livreurDTO.getCoperative() ) );

        return livreur;
    }
}
