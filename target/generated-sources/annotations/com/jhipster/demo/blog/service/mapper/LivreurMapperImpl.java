package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.domain.Livreur;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import com.jhipster.demo.blog.service.dto.LivreurDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-15T22:55:17+0200",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.14.1 (Ubuntu)"
)
@Component
public class LivreurMapperImpl implements LivreurMapper {

    @Override
    public Livreur toEntity(LivreurDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Livreur livreur = new Livreur();

        livreur.setId( dto.getId() );
        livreur.setIdLivreur( dto.getIdLivreur() );
        livreur.setIdCoperative( dto.getIdCoperative() );
        livreur.setNom( dto.getNom() );
        livreur.setPrenom( dto.getPrenom() );
        livreur.setTelephone( dto.getTelephone() );
        livreur.setLatitude( dto.getLatitude() );
        livreur.setLongitude( dto.getLongitude() );
        livreur.setStatutLivreur( dto.getStatutLivreur() );
        livreur.coperative( coperativeDTOToCoperative( dto.getCoperative() ) );

        return livreur;
    }

    @Override
    public List<Livreur> toEntity(List<LivreurDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Livreur> list = new ArrayList<Livreur>( dtoList.size() );
        for ( LivreurDTO livreurDTO : dtoList ) {
            list.add( toEntity( livreurDTO ) );
        }

        return list;
    }

    @Override
    public List<LivreurDTO> toDto(List<Livreur> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<LivreurDTO> list = new ArrayList<LivreurDTO>( entityList.size() );
        for ( Livreur livreur : entityList ) {
            list.add( toDto( livreur ) );
        }

        return list;
    }

    @Override
    public void partialUpdate(Livreur entity, LivreurDTO dto) {
        if ( dto == null ) {
            return;
        }

        if ( dto.getId() != null ) {
            entity.setId( dto.getId() );
        }
        if ( dto.getIdLivreur() != null ) {
            entity.setIdLivreur( dto.getIdLivreur() );
        }
        if ( dto.getIdCoperative() != null ) {
            entity.setIdCoperative( dto.getIdCoperative() );
        }
        if ( dto.getNom() != null ) {
            entity.setNom( dto.getNom() );
        }
        if ( dto.getPrenom() != null ) {
            entity.setPrenom( dto.getPrenom() );
        }
        if ( dto.getTelephone() != null ) {
            entity.setTelephone( dto.getTelephone() );
        }
        if ( dto.getLatitude() != null ) {
            entity.setLatitude( dto.getLatitude() );
        }
        if ( dto.getLongitude() != null ) {
            entity.setLongitude( dto.getLongitude() );
        }
        if ( dto.getStatutLivreur() != null ) {
            entity.setStatutLivreur( dto.getStatutLivreur() );
        }
        if ( dto.getCoperative() != null ) {
            if ( entity.getCoperative() == null ) {
                entity.coperative( new Coperative() );
            }
            coperativeDTOToCoperative1( dto.getCoperative(), entity.getCoperative() );
        }
    }

    @Override
    public LivreurDTO toDto(Livreur s) {
        if ( s == null ) {
            return null;
        }

        LivreurDTO livreurDTO = new LivreurDTO();

        livreurDTO.setCoperative( toDtoCoperativeId( s.getCoperative() ) );
        livreurDTO.setId( s.getId() );
        livreurDTO.setIdLivreur( s.getIdLivreur() );
        livreurDTO.setIdCoperative( s.getIdCoperative() );
        livreurDTO.setNom( s.getNom() );
        livreurDTO.setPrenom( s.getPrenom() );
        livreurDTO.setTelephone( s.getTelephone() );
        livreurDTO.setLatitude( s.getLatitude() );
        livreurDTO.setLongitude( s.getLongitude() );
        livreurDTO.setStatutLivreur( s.getStatutLivreur() );

        return livreurDTO;
    }

    @Override
    public CoperativeDTO toDtoCoperativeId(Coperative coperative) {
        if ( coperative == null ) {
            return null;
        }

        CoperativeDTO coperativeDTO = new CoperativeDTO();

        coperativeDTO.setId( coperative.getId() );

        return coperativeDTO;
    }

    protected Coperative coperativeDTOToCoperative(CoperativeDTO coperativeDTO) {
        if ( coperativeDTO == null ) {
            return null;
        }

        Coperative coperative = new Coperative();

        coperative.setId( coperativeDTO.getId() );
        coperative.setIdCoperative( coperativeDTO.getIdCoperative() );
        coperative.setZoneGeographique( coperativeDTO.getZoneGeographique() );

        return coperative;
    }

    protected void coperativeDTOToCoperative1(CoperativeDTO coperativeDTO, Coperative mappingTarget) {
        if ( coperativeDTO == null ) {
            return;
        }

        if ( coperativeDTO.getId() != null ) {
            mappingTarget.setId( coperativeDTO.getId() );
        }
        if ( coperativeDTO.getIdCoperative() != null ) {
            mappingTarget.setIdCoperative( coperativeDTO.getIdCoperative() );
        }
        if ( coperativeDTO.getZoneGeographique() != null ) {
            mappingTarget.setZoneGeographique( coperativeDTO.getZoneGeographique() );
        }
    }
}
