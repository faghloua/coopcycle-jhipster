package com.jhipster.demo.blog.service.dto;

import com.jhipster.demo.blog.domain.enumeration.StatutLivreur;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jhipster.demo.blog.domain.Livreur} entity.
 */
public class LivreurDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idLivreur;

    private Integer idCoperative;

    @NotNull
    @Size(min = 1, max = 30)
    private String nom;

    @NotNull
    @Size(min = 1, max = 30)
    private String prenom;

    @NotNull
    @Size(min = 10, max = 10)
    private String telephone;

    @NotNull
    private Float latitude;

    @NotNull
    private Float longitude;

    @NotNull
    private StatutLivreur statutLivreur;

    private CoperativeDTO coperative;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdLivreur() {
        return idLivreur;
    }

    public void setIdLivreur(Integer idLivreur) {
        this.idLivreur = idLivreur;
    }

    public Integer getIdCoperative() {
        return idCoperative;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public StatutLivreur getStatutLivreur() {
        return statutLivreur;
    }

    public void setStatutLivreur(StatutLivreur statutLivreur) {
        this.statutLivreur = statutLivreur;
    }

    public CoperativeDTO getCoperative() {
        return coperative;
    }

    public void setCoperative(CoperativeDTO coperative) {
        this.coperative = coperative;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LivreurDTO)) {
            return false;
        }

        LivreurDTO livreurDTO = (LivreurDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, livreurDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LivreurDTO{" +
            "id=" + getId() +
            ", idLivreur=" + getIdLivreur() +
            ", idCoperative=" + getIdCoperative() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", statutLivreur='" + getStatutLivreur() + "'" +
            ", coperative=" + getCoperative() +
            "}";
    }
}
