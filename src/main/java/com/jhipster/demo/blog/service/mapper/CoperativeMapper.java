package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Coperative} and its DTO {@link CoperativeDTO}.
 */
@Mapper(componentModel = "spring")
public interface CoperativeMapper extends EntityMapper<CoperativeDTO, Coperative> {}
