package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.domain.Livreur;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import com.jhipster.demo.blog.service.dto.LivreurDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Livreur} and its DTO {@link LivreurDTO}.
 */
@Mapper(componentModel = "spring")
public interface LivreurMapper extends EntityMapper<LivreurDTO, Livreur> {
    @Mapping(target = "coperative", source = "coperative", qualifiedByName = "coperativeId")
    LivreurDTO toDto(Livreur s);

    @Named("coperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CoperativeDTO toDtoCoperativeId(Coperative coperative);
}
