package com.jhipster.demo.blog.service.dto;

import com.jhipster.demo.blog.domain.enumeration.StatutCommande;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jhipster.demo.blog.domain.Commande} entity.
 */
public class CommandeDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idCommande;

    @NotNull
    private Integer idClient;

    @NotNull
    private Integer idLivreur;

    @NotNull
    private Integer idCoperative;

    @NotNull
    private StatutCommande statutCommande;

    private ClientDTO client;

    private LivreurDTO livreur;

    private Set<ProduitCommandeDTO> produitCommandes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Integer idCommande) {
        this.idCommande = idCommande;
    }

    public Integer getIdClient() {
        return idClient;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getIdLivreur() {
        return idLivreur;
    }

    public void setIdLivreur(Integer idLivreur) {
        this.idLivreur = idLivreur;
    }

    public Integer getIdCoperative() {
        return idCoperative;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public StatutCommande getStatutCommande() {
        return statutCommande;
    }

    public void setStatutCommande(StatutCommande statutCommande) {
        this.statutCommande = statutCommande;
    }

    public ClientDTO getClient() {
        return client;
    }

    public void setClient(ClientDTO client) {
        this.client = client;
    }

    public LivreurDTO getLivreur() {
        return livreur;
    }

    public void setLivreur(LivreurDTO livreur) {
        this.livreur = livreur;
    }

    public Set<ProduitCommandeDTO> getProduitCommandes() {
        return produitCommandes;
    }

    public void setProduitCommandes(Set<ProduitCommandeDTO> produitCommandes) {
        this.produitCommandes = produitCommandes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommandeDTO)) {
            return false;
        }

        CommandeDTO commandeDTO = (CommandeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commandeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommandeDTO{" +
            "id=" + getId() +
            ", idCommande=" + getIdCommande() +
            ", idClient=" + getIdClient() +
            ", idLivreur=" + getIdLivreur() +
            ", idCoperative=" + getIdCoperative() +
            ", statutCommande='" + getStatutCommande() + "'" +
            ", client=" + getClient() +
            ", livreur=" + getLivreur() +
            ", produitCommandes=" + getProduitCommandes() +
            "}";
    }
}
