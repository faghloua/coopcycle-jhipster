package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Commercant;
import com.jhipster.demo.blog.domain.Produit;
import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.service.dto.CommercantDTO;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import com.jhipster.demo.blog.service.dto.ProduitDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Produit} and its DTO {@link ProduitDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProduitMapper extends EntityMapper<ProduitDTO, Produit> {
    @Mapping(target = "produitCommande", source = "produitCommande", qualifiedByName = "produitCommandeId")
    @Mapping(target = "commercant", source = "commercant", qualifiedByName = "commercantId")
    ProduitDTO toDto(Produit s);

    @Named("produitCommandeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProduitCommandeDTO toDtoProduitCommandeId(ProduitCommande produitCommande);

    @Named("commercantId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommercantDTO toDtoCommercantId(Commercant commercant);
}
