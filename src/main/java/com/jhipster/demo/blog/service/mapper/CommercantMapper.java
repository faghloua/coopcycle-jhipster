package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Commercant;
import com.jhipster.demo.blog.service.dto.CommercantDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commercant} and its DTO {@link CommercantDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommercantMapper extends EntityMapper<CommercantDTO, Commercant> {}
