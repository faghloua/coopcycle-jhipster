package com.jhipster.demo.blog.service.dto;

import com.jhipster.demo.blog.domain.enumeration.StatutCommercant;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jhipster.demo.blog.domain.Commercant} entity.
 */
public class CommercantDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idCommercant;

    private String presentation;

    @NotNull
    @Size(min = 10, max = 100)
    private String adresse;

    private String nom;

    private Integer idCoperative;

    @NotNull
    private String disponibilite;

    @NotNull
    private StatutCommercant statutCommercant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdCommercant() {
        return idCommercant;
    }

    public void setIdCommercant(Integer idCommercant) {
        this.idCommercant = idCommercant;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getIdCoperative() {
        return idCoperative;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public String getDisponibilite() {
        return disponibilite;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public StatutCommercant getStatutCommercant() {
        return statutCommercant;
    }

    public void setStatutCommercant(StatutCommercant statutCommercant) {
        this.statutCommercant = statutCommercant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommercantDTO)) {
            return false;
        }

        CommercantDTO commercantDTO = (CommercantDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commercantDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommercantDTO{" +
            "id=" + getId() +
            ", idCommercant=" + getIdCommercant() +
            ", presentation='" + getPresentation() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", nom='" + getNom() + "'" +
            ", idCoperative=" + getIdCoperative() +
            ", disponibilite='" + getDisponibilite() + "'" +
            ", statutCommercant='" + getStatutCommercant() + "'" +
            "}";
    }
}
