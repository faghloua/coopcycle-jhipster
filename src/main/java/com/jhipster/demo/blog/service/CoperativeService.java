package com.jhipster.demo.blog.service;

import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.repository.CoperativeRepository;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import com.jhipster.demo.blog.service.mapper.CoperativeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Coperative}.
 */
@Service
@Transactional
public class CoperativeService {

    private final Logger log = LoggerFactory.getLogger(CoperativeService.class);

    private final CoperativeRepository coperativeRepository;

    private final CoperativeMapper coperativeMapper;

    public CoperativeService(CoperativeRepository coperativeRepository, CoperativeMapper coperativeMapper) {
        this.coperativeRepository = coperativeRepository;
        this.coperativeMapper = coperativeMapper;
    }

    /**
     * Save a coperative.
     *
     * @param coperativeDTO the entity to save.
     * @return the persisted entity.
     */
    public CoperativeDTO save(CoperativeDTO coperativeDTO) {
        log.debug("Request to save Coperative : {}", coperativeDTO);
        Coperative coperative = coperativeMapper.toEntity(coperativeDTO);
        coperative = coperativeRepository.save(coperative);
        return coperativeMapper.toDto(coperative);
    }

    /**
     * Update a coperative.
     *
     * @param coperativeDTO the entity to save.
     * @return the persisted entity.
     */
    public CoperativeDTO update(CoperativeDTO coperativeDTO) {
        log.debug("Request to save Coperative : {}", coperativeDTO);
        Coperative coperative = coperativeMapper.toEntity(coperativeDTO);
        coperative = coperativeRepository.save(coperative);
        return coperativeMapper.toDto(coperative);
    }

    /**
     * Partially update a coperative.
     *
     * @param coperativeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CoperativeDTO> partialUpdate(CoperativeDTO coperativeDTO) {
        log.debug("Request to partially update Coperative : {}", coperativeDTO);

        return coperativeRepository
            .findById(coperativeDTO.getId())
            .map(existingCoperative -> {
                coperativeMapper.partialUpdate(existingCoperative, coperativeDTO);

                return existingCoperative;
            })
            .map(coperativeRepository::save)
            .map(coperativeMapper::toDto);
    }

    /**
     * Get all the coperatives.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CoperativeDTO> findAll() {
        log.debug("Request to get all Coperatives");
        return coperativeRepository.findAll().stream().map(coperativeMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one coperative by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CoperativeDTO> findOne(Long id) {
        log.debug("Request to get Coperative : {}", id);
        return coperativeRepository.findById(id).map(coperativeMapper::toDto);
    }

    /**
     * Delete the coperative by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Coperative : {}", id);
        coperativeRepository.deleteById(id);
    }
}
