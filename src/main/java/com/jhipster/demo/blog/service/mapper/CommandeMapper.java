package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Client;
import com.jhipster.demo.blog.domain.Commande;
import com.jhipster.demo.blog.domain.Livreur;
import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.service.dto.ClientDTO;
import com.jhipster.demo.blog.service.dto.CommandeDTO;
import com.jhipster.demo.blog.service.dto.LivreurDTO;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commande} and its DTO {@link CommandeDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommandeMapper extends EntityMapper<CommandeDTO, Commande> {
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    @Mapping(target = "livreur", source = "livreur", qualifiedByName = "livreurId")
    @Mapping(target = "produitCommandes", source = "produitCommandes", qualifiedByName = "produitCommandeIdSet")
    CommandeDTO toDto(Commande s);

    @Mapping(target = "removeProduitCommande", ignore = true)
    Commande toEntity(CommandeDTO commandeDTO);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("livreurId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    LivreurDTO toDtoLivreurId(Livreur livreur);

    @Named("produitCommandeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProduitCommandeDTO toDtoProduitCommandeId(ProduitCommande produitCommande);

    @Named("produitCommandeIdSet")
    default Set<ProduitCommandeDTO> toDtoProduitCommandeIdSet(Set<ProduitCommande> produitCommande) {
        return produitCommande.stream().map(this::toDtoProduitCommandeId).collect(Collectors.toSet());
    }
}
