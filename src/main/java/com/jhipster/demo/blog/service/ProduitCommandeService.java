package com.jhipster.demo.blog.service;

import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.repository.ProduitCommandeRepository;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import com.jhipster.demo.blog.service.mapper.ProduitCommandeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ProduitCommande}.
 */
@Service
@Transactional
public class ProduitCommandeService {

    private final Logger log = LoggerFactory.getLogger(ProduitCommandeService.class);

    private final ProduitCommandeRepository produitCommandeRepository;

    private final ProduitCommandeMapper produitCommandeMapper;

    public ProduitCommandeService(ProduitCommandeRepository produitCommandeRepository, ProduitCommandeMapper produitCommandeMapper) {
        this.produitCommandeRepository = produitCommandeRepository;
        this.produitCommandeMapper = produitCommandeMapper;
    }

    /**
     * Save a produitCommande.
     *
     * @param produitCommandeDTO the entity to save.
     * @return the persisted entity.
     */
    public ProduitCommandeDTO save(ProduitCommandeDTO produitCommandeDTO) {
        log.debug("Request to save ProduitCommande : {}", produitCommandeDTO);
        ProduitCommande produitCommande = produitCommandeMapper.toEntity(produitCommandeDTO);
        produitCommande = produitCommandeRepository.save(produitCommande);
        return produitCommandeMapper.toDto(produitCommande);
    }

    /**
     * Update a produitCommande.
     *
     * @param produitCommandeDTO the entity to save.
     * @return the persisted entity.
     */
    public ProduitCommandeDTO update(ProduitCommandeDTO produitCommandeDTO) {
        log.debug("Request to save ProduitCommande : {}", produitCommandeDTO);
        ProduitCommande produitCommande = produitCommandeMapper.toEntity(produitCommandeDTO);
        produitCommande = produitCommandeRepository.save(produitCommande);
        return produitCommandeMapper.toDto(produitCommande);
    }

    /**
     * Partially update a produitCommande.
     *
     * @param produitCommandeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ProduitCommandeDTO> partialUpdate(ProduitCommandeDTO produitCommandeDTO) {
        log.debug("Request to partially update ProduitCommande : {}", produitCommandeDTO);

        return produitCommandeRepository
            .findById(produitCommandeDTO.getId())
            .map(existingProduitCommande -> {
                produitCommandeMapper.partialUpdate(existingProduitCommande, produitCommandeDTO);

                return existingProduitCommande;
            })
            .map(produitCommandeRepository::save)
            .map(produitCommandeMapper::toDto);
    }

    /**
     * Get all the produitCommandes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProduitCommandeDTO> findAll() {
        log.debug("Request to get all ProduitCommandes");
        return produitCommandeRepository
            .findAll()
            .stream()
            .map(produitCommandeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get all the produitCommandes where Produit is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProduitCommandeDTO> findAllWhereProduitIsNull() {
        log.debug("Request to get all produitCommandes where Produit is null");
        return StreamSupport
            .stream(produitCommandeRepository.findAll().spliterator(), false)
            .filter(produitCommande -> produitCommande.getProduit() == null)
            .map(produitCommandeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one produitCommande by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProduitCommandeDTO> findOne(Long id) {
        log.debug("Request to get ProduitCommande : {}", id);
        return produitCommandeRepository.findById(id).map(produitCommandeMapper::toDto);
    }

    /**
     * Delete the produitCommande by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProduitCommande : {}", id);
        produitCommandeRepository.deleteById(id);
    }
}
