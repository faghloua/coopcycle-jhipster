package com.jhipster.demo.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jhipster.demo.blog.domain.Produit} entity.
 */
public class ProduitDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idProduit;

    @NotNull
    @Size(min = 1, max = 30)
    private String nomProduit;

    @NotNull
    private Float prix;

    private String description;

    private Integer dispinobilite;

    private ProduitCommandeDTO produitCommande;

    private CommercantDTO commercant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDispinobilite() {
        return dispinobilite;
    }

    public void setDispinobilite(Integer dispinobilite) {
        this.dispinobilite = dispinobilite;
    }

    public ProduitCommandeDTO getProduitCommande() {
        return produitCommande;
    }

    public void setProduitCommande(ProduitCommandeDTO produitCommande) {
        this.produitCommande = produitCommande;
    }

    public CommercantDTO getCommercant() {
        return commercant;
    }

    public void setCommercant(CommercantDTO commercant) {
        this.commercant = commercant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitDTO)) {
            return false;
        }

        ProduitDTO produitDTO = (ProduitDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, produitDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitDTO{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", nomProduit='" + getNomProduit() + "'" +
            ", prix=" + getPrix() +
            ", description='" + getDescription() + "'" +
            ", dispinobilite=" + getDispinobilite() +
            ", produitCommande=" + getProduitCommande() +
            ", commercant=" + getCommercant() +
            "}";
    }
}
