package com.jhipster.demo.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jhipster.demo.blog.domain.Coperative} entity.
 */
public class CoperativeDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idCoperative;

    @NotNull
    private String zoneGeographique;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdCoperative() {
        return idCoperative;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public String getZoneGeographique() {
        return zoneGeographique;
    }

    public void setZoneGeographique(String zoneGeographique) {
        this.zoneGeographique = zoneGeographique;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoperativeDTO)) {
            return false;
        }

        CoperativeDTO coperativeDTO = (CoperativeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, coperativeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CoperativeDTO{" +
            "id=" + getId() +
            ", idCoperative=" + getIdCoperative() +
            ", zoneGeographique='" + getZoneGeographique() + "'" +
            "}";
    }
}
