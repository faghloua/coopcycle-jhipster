package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.ProduitCommande;
import com.jhipster.demo.blog.service.dto.ProduitCommandeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProduitCommande} and its DTO {@link ProduitCommandeDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProduitCommandeMapper extends EntityMapper<ProduitCommandeDTO, ProduitCommande> {}
