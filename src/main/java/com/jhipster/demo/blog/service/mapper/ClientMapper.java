package com.jhipster.demo.blog.service.mapper;

import com.jhipster.demo.blog.domain.Client;
import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.service.dto.ClientDTO;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Client} and its DTO {@link ClientDTO}.
 */
@Mapper(componentModel = "spring")
public interface ClientMapper extends EntityMapper<ClientDTO, Client> {
    @Mapping(target = "coperative", source = "coperative", qualifiedByName = "coperativeId")
    ClientDTO toDto(Client s);

    @Named("coperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CoperativeDTO toDtoCoperativeId(Coperative coperative);
}
