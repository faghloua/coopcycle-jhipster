package com.jhipster.demo.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.jhipster.demo.blog.domain.ProduitCommande} entity.
 */
public class ProduitCommandeDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idProduit;

    @NotNull
    private Integer idCommande;

    @NotNull
    @Max(value = 50)
    private Integer quantite;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public Integer getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Integer idCommande) {
        this.idCommande = idCommande;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitCommandeDTO)) {
            return false;
        }

        ProduitCommandeDTO produitCommandeDTO = (ProduitCommandeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, produitCommandeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitCommandeDTO{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", idCommande=" + getIdCommande() +
            ", quantite=" + getQuantite() +
            "}";
    }
}
