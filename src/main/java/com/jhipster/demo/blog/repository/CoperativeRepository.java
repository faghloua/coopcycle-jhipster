package com.jhipster.demo.blog.repository;

import com.jhipster.demo.blog.domain.Coperative;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Coperative entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoperativeRepository extends JpaRepository<Coperative, Long> {}
