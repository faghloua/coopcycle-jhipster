package com.jhipster.demo.blog.repository;

import com.jhipster.demo.blog.domain.ProduitCommande;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ProduitCommande entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProduitCommandeRepository extends JpaRepository<ProduitCommande, Long> {}
