package com.jhipster.demo.blog.web.rest;

import com.jhipster.demo.blog.repository.CoperativeRepository;
import com.jhipster.demo.blog.service.CoperativeService;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import com.jhipster.demo.blog.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.jhipster.demo.blog.domain.Coperative}.
 */
@RestController
@RequestMapping("/api")
public class CoperativeResource {

    private final Logger log = LoggerFactory.getLogger(CoperativeResource.class);

    private static final String ENTITY_NAME = "coperative";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoperativeService coperativeService;

    private final CoperativeRepository coperativeRepository;

    public CoperativeResource(CoperativeService coperativeService, CoperativeRepository coperativeRepository) {
        this.coperativeService = coperativeService;
        this.coperativeRepository = coperativeRepository;
    }

    /**
     * {@code POST  /coperatives} : Create a new coperative.
     *
     * @param coperativeDTO the coperativeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coperativeDTO, or with status {@code 400 (Bad Request)} if the coperative has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/coperatives")
    public ResponseEntity<CoperativeDTO> createCoperative(@Valid @RequestBody CoperativeDTO coperativeDTO) throws URISyntaxException {
        log.debug("REST request to save Coperative : {}", coperativeDTO);
        if (coperativeDTO.getId() != null) {
            throw new BadRequestAlertException("A new coperative cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CoperativeDTO result = coperativeService.save(coperativeDTO);
        return ResponseEntity
            .created(new URI("/api/coperatives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /coperatives/:id} : Updates an existing coperative.
     *
     * @param id the id of the coperativeDTO to save.
     * @param coperativeDTO the coperativeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coperativeDTO,
     * or with status {@code 400 (Bad Request)} if the coperativeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coperativeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/coperatives/{id}")
    public ResponseEntity<CoperativeDTO> updateCoperative(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CoperativeDTO coperativeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Coperative : {}, {}", id, coperativeDTO);
        if (coperativeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coperativeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coperativeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CoperativeDTO result = coperativeService.update(coperativeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coperativeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /coperatives/:id} : Partial updates given fields of an existing coperative, field will ignore if it is null
     *
     * @param id the id of the coperativeDTO to save.
     * @param coperativeDTO the coperativeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coperativeDTO,
     * or with status {@code 400 (Bad Request)} if the coperativeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the coperativeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the coperativeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/coperatives/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CoperativeDTO> partialUpdateCoperative(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CoperativeDTO coperativeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Coperative partially : {}, {}", id, coperativeDTO);
        if (coperativeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coperativeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coperativeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CoperativeDTO> result = coperativeService.partialUpdate(coperativeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coperativeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /coperatives} : get all the coperatives.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of coperatives in body.
     */
    @GetMapping("/coperatives")
    public List<CoperativeDTO> getAllCoperatives() {
        log.debug("REST request to get all Coperatives");
        return coperativeService.findAll();
    }

    /**
     * {@code GET  /coperatives/:id} : get the "id" coperative.
     *
     * @param id the id of the coperativeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coperativeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/coperatives/{id}")
    public ResponseEntity<CoperativeDTO> getCoperative(@PathVariable Long id) {
        log.debug("REST request to get Coperative : {}", id);
        Optional<CoperativeDTO> coperativeDTO = coperativeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coperativeDTO);
    }

    /**
     * {@code DELETE  /coperatives/:id} : delete the "id" coperative.
     *
     * @param id the id of the coperativeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/coperatives/{id}")
    public ResponseEntity<Void> deleteCoperative(@PathVariable Long id) {
        log.debug("REST request to delete Coperative : {}", id);
        coperativeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
