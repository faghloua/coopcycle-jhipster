package com.jhipster.demo.blog.domain.enumeration;

/**
 * The StatutCommercant enumeration.
 */
public enum StatutCommercant {
    OUVERT,
    FERME,
    OCCUPE,
}
