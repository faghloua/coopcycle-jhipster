package com.jhipster.demo.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ProduitCommande.
 */
@Entity
@Table(name = "produit_commande")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProduitCommande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_produit", nullable = false)
    private Integer idProduit;

    @NotNull
    @Column(name = "id_commande", nullable = false)
    private Integer idCommande;

    @NotNull
    @Max(value = 50)
    @Column(name = "quantite", nullable = false)
    private Integer quantite;

    @JsonIgnoreProperties(value = { "produitCommande", "commercant" }, allowSetters = true)
    @OneToOne(mappedBy = "produitCommande")
    private Produit produit;

    @ManyToMany(mappedBy = "produitCommandes")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "client", "livreur", "produitCommandes" }, allowSetters = true)
    private Set<Commande> commandes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ProduitCommande id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdProduit() {
        return this.idProduit;
    }

    public ProduitCommande idProduit(Integer idProduit) {
        this.setIdProduit(idProduit);
        return this;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public Integer getIdCommande() {
        return this.idCommande;
    }

    public ProduitCommande idCommande(Integer idCommande) {
        this.setIdCommande(idCommande);
        return this;
    }

    public void setIdCommande(Integer idCommande) {
        this.idCommande = idCommande;
    }

    public Integer getQuantite() {
        return this.quantite;
    }

    public ProduitCommande quantite(Integer quantite) {
        this.setQuantite(quantite);
        return this;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Produit getProduit() {
        return this.produit;
    }

    public void setProduit(Produit produit) {
        if (this.produit != null) {
            this.produit.setProduitCommande(null);
        }
        if (produit != null) {
            produit.setProduitCommande(this);
        }
        this.produit = produit;
    }

    public ProduitCommande produit(Produit produit) {
        this.setProduit(produit);
        return this;
    }

    public Set<Commande> getCommandes() {
        return this.commandes;
    }

    public void setCommandes(Set<Commande> commandes) {
        if (this.commandes != null) {
            this.commandes.forEach(i -> i.removeProduitCommande(this));
        }
        if (commandes != null) {
            commandes.forEach(i -> i.addProduitCommande(this));
        }
        this.commandes = commandes;
    }

    public ProduitCommande commandes(Set<Commande> commandes) {
        this.setCommandes(commandes);
        return this;
    }

    public ProduitCommande addCommande(Commande commande) {
        this.commandes.add(commande);
        commande.getProduitCommandes().add(this);
        return this;
    }

    public ProduitCommande removeCommande(Commande commande) {
        this.commandes.remove(commande);
        commande.getProduitCommandes().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitCommande)) {
            return false;
        }
        return id != null && id.equals(((ProduitCommande) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitCommande{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", idCommande=" + getIdCommande() +
            ", quantite=" + getQuantite() +
            "}";
    }
}
