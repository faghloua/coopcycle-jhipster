package com.jhipster.demo.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jhipster.demo.blog.domain.enumeration.StatutCommercant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Commercant.
 */
@Entity
@Table(name = "commercant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Commercant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_commercant", nullable = false)
    private Integer idCommercant;

    @Column(name = "presentation")
    private String presentation;

    @NotNull
    @Size(min = 10, max = 100)
    @Column(name = "adresse", length = 100, nullable = false)
    private String adresse;

    @Column(name = "nom")
    private String nom;

    @Column(name = "id_coperative")
    private Integer idCoperative;

    @NotNull
    @Column(name = "disponibilite", nullable = false)
    private String disponibilite;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "statut_commercant", nullable = false)
    private StatutCommercant statutCommercant;

    @OneToMany(mappedBy = "commercant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produitCommande", "commercant" }, allowSetters = true)
    private Set<Produit> produits = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Commercant id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdCommercant() {
        return this.idCommercant;
    }

    public Commercant idCommercant(Integer idCommercant) {
        this.setIdCommercant(idCommercant);
        return this;
    }

    public void setIdCommercant(Integer idCommercant) {
        this.idCommercant = idCommercant;
    }

    public String getPresentation() {
        return this.presentation;
    }

    public Commercant presentation(String presentation) {
        this.setPresentation(presentation);
        return this;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Commercant adresse(String adresse) {
        this.setAdresse(adresse);
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNom() {
        return this.nom;
    }

    public Commercant nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getIdCoperative() {
        return this.idCoperative;
    }

    public Commercant idCoperative(Integer idCoperative) {
        this.setIdCoperative(idCoperative);
        return this;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public String getDisponibilite() {
        return this.disponibilite;
    }

    public Commercant disponibilite(String disponibilite) {
        this.setDisponibilite(disponibilite);
        return this;
    }

    public void setDisponibilite(String disponibilite) {
        this.disponibilite = disponibilite;
    }

    public StatutCommercant getStatutCommercant() {
        return this.statutCommercant;
    }

    public Commercant statutCommercant(StatutCommercant statutCommercant) {
        this.setStatutCommercant(statutCommercant);
        return this;
    }

    public void setStatutCommercant(StatutCommercant statutCommercant) {
        this.statutCommercant = statutCommercant;
    }

    public Set<Produit> getProduits() {
        return this.produits;
    }

    public void setProduits(Set<Produit> produits) {
        if (this.produits != null) {
            this.produits.forEach(i -> i.setCommercant(null));
        }
        if (produits != null) {
            produits.forEach(i -> i.setCommercant(this));
        }
        this.produits = produits;
    }

    public Commercant produits(Set<Produit> produits) {
        this.setProduits(produits);
        return this;
    }

    public Commercant addProduit(Produit produit) {
        this.produits.add(produit);
        produit.setCommercant(this);
        return this;
    }

    public Commercant removeProduit(Produit produit) {
        this.produits.remove(produit);
        produit.setCommercant(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commercant)) {
            return false;
        }
        return id != null && id.equals(((Commercant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commercant{" +
            "id=" + getId() +
            ", idCommercant=" + getIdCommercant() +
            ", presentation='" + getPresentation() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", nom='" + getNom() + "'" +
            ", idCoperative=" + getIdCoperative() +
            ", disponibilite='" + getDisponibilite() + "'" +
            ", statutCommercant='" + getStatutCommercant() + "'" +
            "}";
    }
}
