package com.jhipster.demo.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jhipster.demo.blog.domain.enumeration.StatutCommande;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Commande.
 */
@Entity
@Table(name = "commande")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_commande", nullable = false)
    private Integer idCommande;

    @NotNull
    @Column(name = "id_client", nullable = false)
    private Integer idClient;

    @NotNull
    @Column(name = "id_livreur", nullable = false)
    private Integer idLivreur;

    @NotNull
    @Column(name = "id_coperative", nullable = false)
    private Integer idCoperative;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "statut_commande", nullable = false)
    private StatutCommande statutCommande;

    @ManyToOne
    @JsonIgnoreProperties(value = { "coperative", "commandes" }, allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = { "coperative", "commandes" }, allowSetters = true)
    private Livreur livreur;

    @ManyToMany
    @JoinTable(
        name = "rel_commande__produit_commande",
        joinColumns = @JoinColumn(name = "commande_id"),
        inverseJoinColumns = @JoinColumn(name = "produit_commande_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produit", "commandes" }, allowSetters = true)
    private Set<ProduitCommande> produitCommandes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Commande id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdCommande() {
        return this.idCommande;
    }

    public Commande idCommande(Integer idCommande) {
        this.setIdCommande(idCommande);
        return this;
    }

    public void setIdCommande(Integer idCommande) {
        this.idCommande = idCommande;
    }

    public Integer getIdClient() {
        return this.idClient;
    }

    public Commande idClient(Integer idClient) {
        this.setIdClient(idClient);
        return this;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getIdLivreur() {
        return this.idLivreur;
    }

    public Commande idLivreur(Integer idLivreur) {
        this.setIdLivreur(idLivreur);
        return this;
    }

    public void setIdLivreur(Integer idLivreur) {
        this.idLivreur = idLivreur;
    }

    public Integer getIdCoperative() {
        return this.idCoperative;
    }

    public Commande idCoperative(Integer idCoperative) {
        this.setIdCoperative(idCoperative);
        return this;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public StatutCommande getStatutCommande() {
        return this.statutCommande;
    }

    public Commande statutCommande(StatutCommande statutCommande) {
        this.setStatutCommande(statutCommande);
        return this;
    }

    public void setStatutCommande(StatutCommande statutCommande) {
        this.statutCommande = statutCommande;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Commande client(Client client) {
        this.setClient(client);
        return this;
    }

    public Livreur getLivreur() {
        return this.livreur;
    }

    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }

    public Commande livreur(Livreur livreur) {
        this.setLivreur(livreur);
        return this;
    }

    public Set<ProduitCommande> getProduitCommandes() {
        return this.produitCommandes;
    }

    public void setProduitCommandes(Set<ProduitCommande> produitCommandes) {
        this.produitCommandes = produitCommandes;
    }

    public Commande produitCommandes(Set<ProduitCommande> produitCommandes) {
        this.setProduitCommandes(produitCommandes);
        return this;
    }

    public Commande addProduitCommande(ProduitCommande produitCommande) {
        this.produitCommandes.add(produitCommande);
        produitCommande.getCommandes().add(this);
        return this;
    }

    public Commande removeProduitCommande(ProduitCommande produitCommande) {
        this.produitCommandes.remove(produitCommande);
        produitCommande.getCommandes().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commande)) {
            return false;
        }
        return id != null && id.equals(((Commande) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commande{" +
            "id=" + getId() +
            ", idCommande=" + getIdCommande() +
            ", idClient=" + getIdClient() +
            ", idLivreur=" + getIdLivreur() +
            ", idCoperative=" + getIdCoperative() +
            ", statutCommande='" + getStatutCommande() + "'" +
            "}";
    }
}
