package com.jhipster.demo.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Coperative.
 */
@Entity
@Table(name = "coperative")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Coperative implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_coperative", nullable = false)
    private Integer idCoperative;

    @NotNull
    @Column(name = "zone_geographique", nullable = false)
    private String zoneGeographique;

    @OneToMany(mappedBy = "coperative")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coperative", "commandes" }, allowSetters = true)
    private Set<Client> clients = new HashSet<>();

    @OneToMany(mappedBy = "coperative")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coperative", "commandes" }, allowSetters = true)
    private Set<Livreur> livreurs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Coperative id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdCoperative() {
        return this.idCoperative;
    }

    public Coperative idCoperative(Integer idCoperative) {
        this.setIdCoperative(idCoperative);
        return this;
    }

    public void setIdCoperative(Integer idCoperative) {
        this.idCoperative = idCoperative;
    }

    public String getZoneGeographique() {
        return this.zoneGeographique;
    }

    public Coperative zoneGeographique(String zoneGeographique) {
        this.setZoneGeographique(zoneGeographique);
        return this;
    }

    public void setZoneGeographique(String zoneGeographique) {
        this.zoneGeographique = zoneGeographique;
    }

    public Set<Client> getClients() {
        return this.clients;
    }

    public void setClients(Set<Client> clients) {
        if (this.clients != null) {
            this.clients.forEach(i -> i.setCoperative(null));
        }
        if (clients != null) {
            clients.forEach(i -> i.setCoperative(this));
        }
        this.clients = clients;
    }

    public Coperative clients(Set<Client> clients) {
        this.setClients(clients);
        return this;
    }

    public Coperative addClient(Client client) {
        this.clients.add(client);
        client.setCoperative(this);
        return this;
    }

    public Coperative removeClient(Client client) {
        this.clients.remove(client);
        client.setCoperative(null);
        return this;
    }

    public Set<Livreur> getLivreurs() {
        return this.livreurs;
    }

    public void setLivreurs(Set<Livreur> livreurs) {
        if (this.livreurs != null) {
            this.livreurs.forEach(i -> i.setCoperative(null));
        }
        if (livreurs != null) {
            livreurs.forEach(i -> i.setCoperative(this));
        }
        this.livreurs = livreurs;
    }

    public Coperative livreurs(Set<Livreur> livreurs) {
        this.setLivreurs(livreurs);
        return this;
    }

    public Coperative addLivreur(Livreur livreur) {
        this.livreurs.add(livreur);
        livreur.setCoperative(this);
        return this;
    }

    public Coperative removeLivreur(Livreur livreur) {
        this.livreurs.remove(livreur);
        livreur.setCoperative(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coperative)) {
            return false;
        }
        return id != null && id.equals(((Coperative) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Coperative{" +
            "id=" + getId() +
            ", idCoperative=" + getIdCoperative() +
            ", zoneGeographique='" + getZoneGeographique() + "'" +
            "}";
    }
}
