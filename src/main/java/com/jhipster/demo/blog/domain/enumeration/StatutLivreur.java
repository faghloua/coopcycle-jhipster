package com.jhipster.demo.blog.domain.enumeration;

/**
 * The StatutLivreur enumeration.
 */
public enum StatutLivreur {
    DISPONIBLE,
    OCCUPE,
}
