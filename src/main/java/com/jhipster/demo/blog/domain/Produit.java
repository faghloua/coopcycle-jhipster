package com.jhipster.demo.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_produit", nullable = false)
    private Integer idProduit;

    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nom_produit", length = 30, nullable = false)
    private String nomProduit;

    @NotNull
    @Column(name = "prix", nullable = false)
    private Float prix;

    @Column(name = "description")
    private String description;

    @Column(name = "dispinobilite")
    private Integer dispinobilite;

    @JsonIgnoreProperties(value = { "produit", "commandes" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private ProduitCommande produitCommande;

    @ManyToOne
    @JsonIgnoreProperties(value = { "produits" }, allowSetters = true)
    private Commercant commercant;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Produit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdProduit() {
        return this.idProduit;
    }

    public Produit idProduit(Integer idProduit) {
        this.setIdProduit(idProduit);
        return this;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public String getNomProduit() {
        return this.nomProduit;
    }

    public Produit nomProduit(String nomProduit) {
        this.setNomProduit(nomProduit);
        return this;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public Float getPrix() {
        return this.prix;
    }

    public Produit prix(Float prix) {
        this.setPrix(prix);
        return this;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return this.description;
    }

    public Produit description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDispinobilite() {
        return this.dispinobilite;
    }

    public Produit dispinobilite(Integer dispinobilite) {
        this.setDispinobilite(dispinobilite);
        return this;
    }

    public void setDispinobilite(Integer dispinobilite) {
        this.dispinobilite = dispinobilite;
    }

    public ProduitCommande getProduitCommande() {
        return this.produitCommande;
    }

    public void setProduitCommande(ProduitCommande produitCommande) {
        this.produitCommande = produitCommande;
    }

    public Produit produitCommande(ProduitCommande produitCommande) {
        this.setProduitCommande(produitCommande);
        return this;
    }

    public Commercant getCommercant() {
        return this.commercant;
    }

    public void setCommercant(Commercant commercant) {
        this.commercant = commercant;
    }

    public Produit commercant(Commercant commercant) {
        this.setCommercant(commercant);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Produit{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", nomProduit='" + getNomProduit() + "'" +
            ", prix=" + getPrix() +
            ", description='" + getDescription() + "'" +
            ", dispinobilite=" + getDispinobilite() +
            "}";
    }
}
