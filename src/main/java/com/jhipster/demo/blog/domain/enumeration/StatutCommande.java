package com.jhipster.demo.blog.domain.enumeration;

/**
 * The StatutCommande enumeration.
 */
public enum StatutCommande {
    EN_ATTENTE_DE_CONFIRMATION,
    EN_COURS_DE_PREPARATION,
    PRETE,
    EN_COURS_DE_LIVRAISON,
    LIVRE,
    RECHERCHE_DE_LIVREUR,
}
