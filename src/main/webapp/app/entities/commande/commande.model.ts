import { IClient } from 'app/entities/client/client.model';
import { ILivreur } from 'app/entities/livreur/livreur.model';
import { IProduitCommande } from 'app/entities/produit-commande/produit-commande.model';
import { StatutCommande } from 'app/entities/enumerations/statut-commande.model';

export interface ICommande {
  id?: number;
  idCommande?: number;
  idClient?: number;
  idLivreur?: number;
  idCoperative?: number;
  statutCommande?: StatutCommande;
  client?: IClient | null;
  livreur?: ILivreur | null;
  produitCommandes?: IProduitCommande[] | null;
}

export class Commande implements ICommande {
  constructor(
    public id?: number,
    public idCommande?: number,
    public idClient?: number,
    public idLivreur?: number,
    public idCoperative?: number,
    public statutCommande?: StatutCommande,
    public client?: IClient | null,
    public livreur?: ILivreur | null,
    public produitCommandes?: IProduitCommande[] | null
  ) {}
}

export function getCommandeIdentifier(commande: ICommande): number | undefined {
  return commande.id;
}
