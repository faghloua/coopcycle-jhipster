import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICommande, Commande } from '../commande.model';
import { CommandeService } from '../service/commande.service';
import { IClient } from 'app/entities/client/client.model';
import { ClientService } from 'app/entities/client/service/client.service';
import { ILivreur } from 'app/entities/livreur/livreur.model';
import { LivreurService } from 'app/entities/livreur/service/livreur.service';
import { IProduitCommande } from 'app/entities/produit-commande/produit-commande.model';
import { ProduitCommandeService } from 'app/entities/produit-commande/service/produit-commande.service';
import { StatutCommande } from 'app/entities/enumerations/statut-commande.model';

@Component({
  selector: 'jhi-commande-update',
  templateUrl: './commande-update.component.html',
})
export class CommandeUpdateComponent implements OnInit {
  isSaving = false;
  statutCommandeValues = Object.keys(StatutCommande);

  clientsSharedCollection: IClient[] = [];
  livreursSharedCollection: ILivreur[] = [];
  produitCommandesSharedCollection: IProduitCommande[] = [];

  editForm = this.fb.group({
    id: [],
    idCommande: [null, [Validators.required]],
    idClient: [null, [Validators.required]],
    idLivreur: [null, [Validators.required]],
    idCoperative: [null, [Validators.required]],
    statutCommande: [null, [Validators.required]],
    client: [],
    livreur: [],
    produitCommandes: [],
  });

  constructor(
    protected commandeService: CommandeService,
    protected clientService: ClientService,
    protected livreurService: LivreurService,
    protected produitCommandeService: ProduitCommandeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commande }) => {
      this.updateForm(commande);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commande = this.createFromForm();
    if (commande.id !== undefined) {
      this.subscribeToSaveResponse(this.commandeService.update(commande));
    } else {
      this.subscribeToSaveResponse(this.commandeService.create(commande));
    }
  }

  trackClientById(_index: number, item: IClient): number {
    return item.id!;
  }

  trackLivreurById(_index: number, item: ILivreur): number {
    return item.id!;
  }

  trackProduitCommandeById(_index: number, item: IProduitCommande): number {
    return item.id!;
  }

  getSelectedProduitCommande(option: IProduitCommande, selectedVals?: IProduitCommande[]): IProduitCommande {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommande>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(commande: ICommande): void {
    this.editForm.patchValue({
      id: commande.id,
      idCommande: commande.idCommande,
      idClient: commande.idClient,
      idLivreur: commande.idLivreur,
      idCoperative: commande.idCoperative,
      statutCommande: commande.statutCommande,
      client: commande.client,
      livreur: commande.livreur,
      produitCommandes: commande.produitCommandes,
    });

    this.clientsSharedCollection = this.clientService.addClientToCollectionIfMissing(this.clientsSharedCollection, commande.client);
    this.livreursSharedCollection = this.livreurService.addLivreurToCollectionIfMissing(this.livreursSharedCollection, commande.livreur);
    this.produitCommandesSharedCollection = this.produitCommandeService.addProduitCommandeToCollectionIfMissing(
      this.produitCommandesSharedCollection,
      ...(commande.produitCommandes ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.clientService
      .query()
      .pipe(map((res: HttpResponse<IClient[]>) => res.body ?? []))
      .pipe(map((clients: IClient[]) => this.clientService.addClientToCollectionIfMissing(clients, this.editForm.get('client')!.value)))
      .subscribe((clients: IClient[]) => (this.clientsSharedCollection = clients));

    this.livreurService
      .query()
      .pipe(map((res: HttpResponse<ILivreur[]>) => res.body ?? []))
      .pipe(
        map((livreurs: ILivreur[]) => this.livreurService.addLivreurToCollectionIfMissing(livreurs, this.editForm.get('livreur')!.value))
      )
      .subscribe((livreurs: ILivreur[]) => (this.livreursSharedCollection = livreurs));

    this.produitCommandeService
      .query()
      .pipe(map((res: HttpResponse<IProduitCommande[]>) => res.body ?? []))
      .pipe(
        map((produitCommandes: IProduitCommande[]) =>
          this.produitCommandeService.addProduitCommandeToCollectionIfMissing(
            produitCommandes,
            ...(this.editForm.get('produitCommandes')!.value ?? [])
          )
        )
      )
      .subscribe((produitCommandes: IProduitCommande[]) => (this.produitCommandesSharedCollection = produitCommandes));
  }

  protected createFromForm(): ICommande {
    return {
      ...new Commande(),
      id: this.editForm.get(['id'])!.value,
      idCommande: this.editForm.get(['idCommande'])!.value,
      idClient: this.editForm.get(['idClient'])!.value,
      idLivreur: this.editForm.get(['idLivreur'])!.value,
      idCoperative: this.editForm.get(['idCoperative'])!.value,
      statutCommande: this.editForm.get(['statutCommande'])!.value,
      client: this.editForm.get(['client'])!.value,
      livreur: this.editForm.get(['livreur'])!.value,
      produitCommandes: this.editForm.get(['produitCommandes'])!.value,
    };
  }
}
