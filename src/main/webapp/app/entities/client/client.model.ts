import { ICoperative } from 'app/entities/coperative/coperative.model';
import { ICommande } from 'app/entities/commande/commande.model';

export interface IClient {
  id?: number;
  idClient?: number;
  idCoperative?: number | null;
  nom?: string;
  prenom?: string;
  telephone?: string;
  adresse?: string;
  coperative?: ICoperative | null;
  commandes?: ICommande[] | null;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public idClient?: number,
    public idCoperative?: number | null,
    public nom?: string,
    public prenom?: string,
    public telephone?: string,
    public adresse?: string,
    public coperative?: ICoperative | null,
    public commandes?: ICommande[] | null
  ) {}
}

export function getClientIdentifier(client: IClient): number | undefined {
  return client.id;
}
