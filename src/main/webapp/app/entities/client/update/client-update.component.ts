import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IClient, Client } from '../client.model';
import { ClientService } from '../service/client.service';
import { ICoperative } from 'app/entities/coperative/coperative.model';
import { CoperativeService } from 'app/entities/coperative/service/coperative.service';

@Component({
  selector: 'jhi-client-update',
  templateUrl: './client-update.component.html',
})
export class ClientUpdateComponent implements OnInit {
  isSaving = false;

  coperativesSharedCollection: ICoperative[] = [];

  editForm = this.fb.group({
    id: [],
    idClient: [null, [Validators.required]],
    idCoperative: [],
    nom: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    prenom: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    telephone: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
    adresse: [null, [Validators.required]],
    coperative: [],
  });

  constructor(
    protected clientService: ClientService,
    protected coperativeService: CoperativeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ client }) => {
      this.updateForm(client);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const client = this.createFromForm();
    if (client.id !== undefined) {
      this.subscribeToSaveResponse(this.clientService.update(client));
    } else {
      this.subscribeToSaveResponse(this.clientService.create(client));
    }
  }

  trackCoperativeById(_index: number, item: ICoperative): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IClient>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(client: IClient): void {
    this.editForm.patchValue({
      id: client.id,
      idClient: client.idClient,
      idCoperative: client.idCoperative,
      nom: client.nom,
      prenom: client.prenom,
      telephone: client.telephone,
      adresse: client.adresse,
      coperative: client.coperative,
    });

    this.coperativesSharedCollection = this.coperativeService.addCoperativeToCollectionIfMissing(
      this.coperativesSharedCollection,
      client.coperative
    );
  }

  protected loadRelationshipsOptions(): void {
    this.coperativeService
      .query()
      .pipe(map((res: HttpResponse<ICoperative[]>) => res.body ?? []))
      .pipe(
        map((coperatives: ICoperative[]) =>
          this.coperativeService.addCoperativeToCollectionIfMissing(coperatives, this.editForm.get('coperative')!.value)
        )
      )
      .subscribe((coperatives: ICoperative[]) => (this.coperativesSharedCollection = coperatives));
  }

  protected createFromForm(): IClient {
    return {
      ...new Client(),
      id: this.editForm.get(['id'])!.value,
      idClient: this.editForm.get(['idClient'])!.value,
      idCoperative: this.editForm.get(['idCoperative'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      coperative: this.editForm.get(['coperative'])!.value,
    };
  }
}
