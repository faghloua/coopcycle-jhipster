import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoperative } from '../coperative.model';
import { CoperativeService } from '../service/coperative.service';
import { CoperativeDeleteDialogComponent } from '../delete/coperative-delete-dialog.component';

@Component({
  selector: 'jhi-coperative',
  templateUrl: './coperative.component.html',
})
export class CoperativeComponent implements OnInit {
  coperatives?: ICoperative[];
  isLoading = false;

  constructor(protected coperativeService: CoperativeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.coperativeService.query().subscribe({
      next: (res: HttpResponse<ICoperative[]>) => {
        this.isLoading = false;
        this.coperatives = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: ICoperative): number {
    return item.id!;
  }

  delete(coperative: ICoperative): void {
    const modalRef = this.modalService.open(CoperativeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.coperative = coperative;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
