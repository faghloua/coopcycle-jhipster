import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICoperative, Coperative } from '../coperative.model';
import { CoperativeService } from '../service/coperative.service';

@Injectable({ providedIn: 'root' })
export class CoperativeRoutingResolveService implements Resolve<ICoperative> {
  constructor(protected service: CoperativeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoperative> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((coperative: HttpResponse<Coperative>) => {
          if (coperative.body) {
            return of(coperative.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Coperative());
  }
}
