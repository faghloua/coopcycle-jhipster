import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CoperativeComponent } from '../list/coperative.component';
import { CoperativeDetailComponent } from '../detail/coperative-detail.component';
import { CoperativeUpdateComponent } from '../update/coperative-update.component';
import { CoperativeRoutingResolveService } from './coperative-routing-resolve.service';

const coperativeRoute: Routes = [
  {
    path: '',
    component: CoperativeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CoperativeDetailComponent,
    resolve: {
      coperative: CoperativeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CoperativeUpdateComponent,
    resolve: {
      coperative: CoperativeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CoperativeUpdateComponent,
    resolve: {
      coperative: CoperativeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(coperativeRoute)],
  exports: [RouterModule],
})
export class CoperativeRoutingModule {}
