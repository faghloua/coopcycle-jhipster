import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoperative } from '../coperative.model';
import { CoperativeService } from '../service/coperative.service';

@Component({
  templateUrl: './coperative-delete-dialog.component.html',
})
export class CoperativeDeleteDialogComponent {
  coperative?: ICoperative;

  constructor(protected coperativeService: CoperativeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.coperativeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
