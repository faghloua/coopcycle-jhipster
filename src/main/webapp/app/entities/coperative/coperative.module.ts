import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CoperativeComponent } from './list/coperative.component';
import { CoperativeDetailComponent } from './detail/coperative-detail.component';
import { CoperativeUpdateComponent } from './update/coperative-update.component';
import { CoperativeDeleteDialogComponent } from './delete/coperative-delete-dialog.component';
import { CoperativeRoutingModule } from './route/coperative-routing.module';

@NgModule({
  imports: [SharedModule, CoperativeRoutingModule],
  declarations: [CoperativeComponent, CoperativeDetailComponent, CoperativeUpdateComponent, CoperativeDeleteDialogComponent],
  entryComponents: [CoperativeDeleteDialogComponent],
})
export class CoperativeModule {}
