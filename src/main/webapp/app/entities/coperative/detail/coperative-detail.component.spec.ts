import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoperativeDetailComponent } from './coperative-detail.component';

describe('Coperative Management Detail Component', () => {
  let comp: CoperativeDetailComponent;
  let fixture: ComponentFixture<CoperativeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoperativeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ coperative: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CoperativeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CoperativeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load coperative on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.coperative).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
