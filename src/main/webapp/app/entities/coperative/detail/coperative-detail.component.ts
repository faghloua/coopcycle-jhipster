import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICoperative } from '../coperative.model';

@Component({
  selector: 'jhi-coperative-detail',
  templateUrl: './coperative-detail.component.html',
})
export class CoperativeDetailComponent implements OnInit {
  coperative: ICoperative | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coperative }) => {
      this.coperative = coperative;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
