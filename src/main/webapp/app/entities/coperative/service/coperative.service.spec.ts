import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICoperative, Coperative } from '../coperative.model';

import { CoperativeService } from './coperative.service';

describe('Coperative Service', () => {
  let service: CoperativeService;
  let httpMock: HttpTestingController;
  let elemDefault: ICoperative;
  let expectedResult: ICoperative | ICoperative[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CoperativeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      idCoperative: 0,
      zoneGeographique: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Coperative', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Coperative()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Coperative', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          idCoperative: 1,
          zoneGeographique: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Coperative', () => {
      const patchObject = Object.assign({}, new Coperative());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Coperative', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          idCoperative: 1,
          zoneGeographique: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Coperative', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCoperativeToCollectionIfMissing', () => {
      it('should add a Coperative to an empty array', () => {
        const coperative: ICoperative = { id: 123 };
        expectedResult = service.addCoperativeToCollectionIfMissing([], coperative);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coperative);
      });

      it('should not add a Coperative to an array that contains it', () => {
        const coperative: ICoperative = { id: 123 };
        const coperativeCollection: ICoperative[] = [
          {
            ...coperative,
          },
          { id: 456 },
        ];
        expectedResult = service.addCoperativeToCollectionIfMissing(coperativeCollection, coperative);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Coperative to an array that doesn't contain it", () => {
        const coperative: ICoperative = { id: 123 };
        const coperativeCollection: ICoperative[] = [{ id: 456 }];
        expectedResult = service.addCoperativeToCollectionIfMissing(coperativeCollection, coperative);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coperative);
      });

      it('should add only unique Coperative to an array', () => {
        const coperativeArray: ICoperative[] = [{ id: 123 }, { id: 456 }, { id: 23981 }];
        const coperativeCollection: ICoperative[] = [{ id: 123 }];
        expectedResult = service.addCoperativeToCollectionIfMissing(coperativeCollection, ...coperativeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const coperative: ICoperative = { id: 123 };
        const coperative2: ICoperative = { id: 456 };
        expectedResult = service.addCoperativeToCollectionIfMissing([], coperative, coperative2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coperative);
        expect(expectedResult).toContain(coperative2);
      });

      it('should accept null and undefined values', () => {
        const coperative: ICoperative = { id: 123 };
        expectedResult = service.addCoperativeToCollectionIfMissing([], null, coperative, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coperative);
      });

      it('should return initial array if no Coperative is added', () => {
        const coperativeCollection: ICoperative[] = [{ id: 123 }];
        expectedResult = service.addCoperativeToCollectionIfMissing(coperativeCollection, undefined, null);
        expect(expectedResult).toEqual(coperativeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
