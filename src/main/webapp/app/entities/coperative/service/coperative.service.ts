import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICoperative, getCoperativeIdentifier } from '../coperative.model';

export type EntityResponseType = HttpResponse<ICoperative>;
export type EntityArrayResponseType = HttpResponse<ICoperative[]>;

@Injectable({ providedIn: 'root' })
export class CoperativeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/coperatives');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(coperative: ICoperative): Observable<EntityResponseType> {
    return this.http.post<ICoperative>(this.resourceUrl, coperative, { observe: 'response' });
  }

  update(coperative: ICoperative): Observable<EntityResponseType> {
    return this.http.put<ICoperative>(`${this.resourceUrl}/${getCoperativeIdentifier(coperative) as number}`, coperative, {
      observe: 'response',
    });
  }

  partialUpdate(coperative: ICoperative): Observable<EntityResponseType> {
    return this.http.patch<ICoperative>(`${this.resourceUrl}/${getCoperativeIdentifier(coperative) as number}`, coperative, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICoperative>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoperative[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCoperativeToCollectionIfMissing(
    coperativeCollection: ICoperative[],
    ...coperativesToCheck: (ICoperative | null | undefined)[]
  ): ICoperative[] {
    const coperatives: ICoperative[] = coperativesToCheck.filter(isPresent);
    if (coperatives.length > 0) {
      const coperativeCollectionIdentifiers = coperativeCollection.map(coperativeItem => getCoperativeIdentifier(coperativeItem)!);
      const coperativesToAdd = coperatives.filter(coperativeItem => {
        const coperativeIdentifier = getCoperativeIdentifier(coperativeItem);
        if (coperativeIdentifier == null || coperativeCollectionIdentifiers.includes(coperativeIdentifier)) {
          return false;
        }
        coperativeCollectionIdentifiers.push(coperativeIdentifier);
        return true;
      });
      return [...coperativesToAdd, ...coperativeCollection];
    }
    return coperativeCollection;
  }
}
