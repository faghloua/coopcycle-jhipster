import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICoperative, Coperative } from '../coperative.model';
import { CoperativeService } from '../service/coperative.service';

@Component({
  selector: 'jhi-coperative-update',
  templateUrl: './coperative-update.component.html',
})
export class CoperativeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    idCoperative: [null, [Validators.required]],
    zoneGeographique: [null, [Validators.required]],
  });

  constructor(protected coperativeService: CoperativeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coperative }) => {
      this.updateForm(coperative);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coperative = this.createFromForm();
    if (coperative.id !== undefined) {
      this.subscribeToSaveResponse(this.coperativeService.update(coperative));
    } else {
      this.subscribeToSaveResponse(this.coperativeService.create(coperative));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoperative>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coperative: ICoperative): void {
    this.editForm.patchValue({
      id: coperative.id,
      idCoperative: coperative.idCoperative,
      zoneGeographique: coperative.zoneGeographique,
    });
  }

  protected createFromForm(): ICoperative {
    return {
      ...new Coperative(),
      id: this.editForm.get(['id'])!.value,
      idCoperative: this.editForm.get(['idCoperative'])!.value,
      zoneGeographique: this.editForm.get(['zoneGeographique'])!.value,
    };
  }
}
