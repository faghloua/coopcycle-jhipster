import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CoperativeService } from '../service/coperative.service';
import { ICoperative, Coperative } from '../coperative.model';

import { CoperativeUpdateComponent } from './coperative-update.component';

describe('Coperative Management Update Component', () => {
  let comp: CoperativeUpdateComponent;
  let fixture: ComponentFixture<CoperativeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let coperativeService: CoperativeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CoperativeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CoperativeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CoperativeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    coperativeService = TestBed.inject(CoperativeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const coperative: ICoperative = { id: 456 };

      activatedRoute.data = of({ coperative });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(coperative));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Coperative>>();
      const coperative = { id: 123 };
      jest.spyOn(coperativeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coperative }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(coperativeService.update).toHaveBeenCalledWith(coperative);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Coperative>>();
      const coperative = new Coperative();
      jest.spyOn(coperativeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coperative }));
      saveSubject.complete();

      // THEN
      expect(coperativeService.create).toHaveBeenCalledWith(coperative);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Coperative>>();
      const coperative = { id: 123 };
      jest.spyOn(coperativeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coperative });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(coperativeService.update).toHaveBeenCalledWith(coperative);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
