import { IClient } from 'app/entities/client/client.model';
import { ILivreur } from 'app/entities/livreur/livreur.model';

export interface ICoperative {
  id?: number;
  idCoperative?: number;
  zoneGeographique?: string;
  clients?: IClient[] | null;
  livreurs?: ILivreur[] | null;
}

export class Coperative implements ICoperative {
  constructor(
    public id?: number,
    public idCoperative?: number,
    public zoneGeographique?: string,
    public clients?: IClient[] | null,
    public livreurs?: ILivreur[] | null
  ) {}
}

export function getCoperativeIdentifier(coperative: ICoperative): number | undefined {
  return coperative.id;
}
