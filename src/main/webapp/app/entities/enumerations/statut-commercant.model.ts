export enum StatutCommercant {
  OUVERT = 'OUVERT',

  FERME = 'FERME',

  OCCUPE = 'OCCUPE',
}
