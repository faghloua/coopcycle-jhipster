import { IProduit } from 'app/entities/produit/produit.model';
import { StatutCommercant } from 'app/entities/enumerations/statut-commercant.model';

export interface ICommercant {
  id?: number;
  idCommercant?: number;
  presentation?: string | null;
  adresse?: string;
  nom?: string | null;
  idCoperative?: number | null;
  disponibilite?: string;
  statutCommercant?: StatutCommercant;
  produits?: IProduit[] | null;
}

export class Commercant implements ICommercant {
  constructor(
    public id?: number,
    public idCommercant?: number,
    public presentation?: string | null,
    public adresse?: string,
    public nom?: string | null,
    public idCoperative?: number | null,
    public disponibilite?: string,
    public statutCommercant?: StatutCommercant,
    public produits?: IProduit[] | null
  ) {}
}

export function getCommercantIdentifier(commercant: ICommercant): number | undefined {
  return commercant.id;
}
