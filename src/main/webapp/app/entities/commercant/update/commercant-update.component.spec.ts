import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CommercantService } from '../service/commercant.service';
import { ICommercant, Commercant } from '../commercant.model';

import { CommercantUpdateComponent } from './commercant-update.component';

describe('Commercant Management Update Component', () => {
  let comp: CommercantUpdateComponent;
  let fixture: ComponentFixture<CommercantUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let commercantService: CommercantService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CommercantUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CommercantUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CommercantUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    commercantService = TestBed.inject(CommercantService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const commercant: ICommercant = { id: 456 };

      activatedRoute.data = of({ commercant });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(commercant));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Commercant>>();
      const commercant = { id: 123 };
      jest.spyOn(commercantService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commercant });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commercant }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(commercantService.update).toHaveBeenCalledWith(commercant);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Commercant>>();
      const commercant = new Commercant();
      jest.spyOn(commercantService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commercant });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commercant }));
      saveSubject.complete();

      // THEN
      expect(commercantService.create).toHaveBeenCalledWith(commercant);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Commercant>>();
      const commercant = { id: 123 };
      jest.spyOn(commercantService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commercant });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(commercantService.update).toHaveBeenCalledWith(commercant);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
