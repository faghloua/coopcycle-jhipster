import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICommercant, Commercant } from '../commercant.model';
import { CommercantService } from '../service/commercant.service';
import { StatutCommercant } from 'app/entities/enumerations/statut-commercant.model';

@Component({
  selector: 'jhi-commercant-update',
  templateUrl: './commercant-update.component.html',
})
export class CommercantUpdateComponent implements OnInit {
  isSaving = false;
  statutCommercantValues = Object.keys(StatutCommercant);

  editForm = this.fb.group({
    id: [],
    idCommercant: [null, [Validators.required]],
    presentation: [],
    adresse: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(100)]],
    nom: [],
    idCoperative: [],
    disponibilite: [null, [Validators.required]],
    statutCommercant: [null, [Validators.required]],
  });

  constructor(protected commercantService: CommercantService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commercant }) => {
      this.updateForm(commercant);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commercant = this.createFromForm();
    if (commercant.id !== undefined) {
      this.subscribeToSaveResponse(this.commercantService.update(commercant));
    } else {
      this.subscribeToSaveResponse(this.commercantService.create(commercant));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommercant>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(commercant: ICommercant): void {
    this.editForm.patchValue({
      id: commercant.id,
      idCommercant: commercant.idCommercant,
      presentation: commercant.presentation,
      adresse: commercant.adresse,
      nom: commercant.nom,
      idCoperative: commercant.idCoperative,
      disponibilite: commercant.disponibilite,
      statutCommercant: commercant.statutCommercant,
    });
  }

  protected createFromForm(): ICommercant {
    return {
      ...new Commercant(),
      id: this.editForm.get(['id'])!.value,
      idCommercant: this.editForm.get(['idCommercant'])!.value,
      presentation: this.editForm.get(['presentation'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      idCoperative: this.editForm.get(['idCoperative'])!.value,
      disponibilite: this.editForm.get(['disponibilite'])!.value,
      statutCommercant: this.editForm.get(['statutCommercant'])!.value,
    };
  }
}
