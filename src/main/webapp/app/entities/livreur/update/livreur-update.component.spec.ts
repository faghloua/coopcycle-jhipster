import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { LivreurService } from '../service/livreur.service';
import { ILivreur, Livreur } from '../livreur.model';
import { ICoperative } from 'app/entities/coperative/coperative.model';
import { CoperativeService } from 'app/entities/coperative/service/coperative.service';

import { LivreurUpdateComponent } from './livreur-update.component';

describe('Livreur Management Update Component', () => {
  let comp: LivreurUpdateComponent;
  let fixture: ComponentFixture<LivreurUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let livreurService: LivreurService;
  let coperativeService: CoperativeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [LivreurUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(LivreurUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(LivreurUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    livreurService = TestBed.inject(LivreurService);
    coperativeService = TestBed.inject(CoperativeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Coperative query and add missing value', () => {
      const livreur: ILivreur = { id: 456 };
      const coperative: ICoperative = { id: 77212 };
      livreur.coperative = coperative;

      const coperativeCollection: ICoperative[] = [{ id: 3726 }];
      jest.spyOn(coperativeService, 'query').mockReturnValue(of(new HttpResponse({ body: coperativeCollection })));
      const additionalCoperatives = [coperative];
      const expectedCollection: ICoperative[] = [...additionalCoperatives, ...coperativeCollection];
      jest.spyOn(coperativeService, 'addCoperativeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ livreur });
      comp.ngOnInit();

      expect(coperativeService.query).toHaveBeenCalled();
      expect(coperativeService.addCoperativeToCollectionIfMissing).toHaveBeenCalledWith(coperativeCollection, ...additionalCoperatives);
      expect(comp.coperativesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const livreur: ILivreur = { id: 456 };
      const coperative: ICoperative = { id: 28394 };
      livreur.coperative = coperative;

      activatedRoute.data = of({ livreur });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(livreur));
      expect(comp.coperativesSharedCollection).toContain(coperative);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Livreur>>();
      const livreur = { id: 123 };
      jest.spyOn(livreurService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ livreur });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: livreur }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(livreurService.update).toHaveBeenCalledWith(livreur);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Livreur>>();
      const livreur = new Livreur();
      jest.spyOn(livreurService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ livreur });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: livreur }));
      saveSubject.complete();

      // THEN
      expect(livreurService.create).toHaveBeenCalledWith(livreur);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Livreur>>();
      const livreur = { id: 123 };
      jest.spyOn(livreurService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ livreur });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(livreurService.update).toHaveBeenCalledWith(livreur);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCoperativeById', () => {
      it('Should return tracked Coperative primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCoperativeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
