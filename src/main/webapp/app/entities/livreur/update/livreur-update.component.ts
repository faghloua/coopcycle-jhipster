import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ILivreur, Livreur } from '../livreur.model';
import { LivreurService } from '../service/livreur.service';
import { ICoperative } from 'app/entities/coperative/coperative.model';
import { CoperativeService } from 'app/entities/coperative/service/coperative.service';
import { StatutLivreur } from 'app/entities/enumerations/statut-livreur.model';

@Component({
  selector: 'jhi-livreur-update',
  templateUrl: './livreur-update.component.html',
})
export class LivreurUpdateComponent implements OnInit {
  isSaving = false;
  statutLivreurValues = Object.keys(StatutLivreur);

  coperativesSharedCollection: ICoperative[] = [];

  editForm = this.fb.group({
    id: [],
    idLivreur: [null, [Validators.required]],
    idCoperative: [],
    nom: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    prenom: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    telephone: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
    latitude: [null, [Validators.required]],
    longitude: [null, [Validators.required]],
    statutLivreur: [null, [Validators.required]],
    coperative: [],
  });

  constructor(
    protected livreurService: LivreurService,
    protected coperativeService: CoperativeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ livreur }) => {
      this.updateForm(livreur);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const livreur = this.createFromForm();
    if (livreur.id !== undefined) {
      this.subscribeToSaveResponse(this.livreurService.update(livreur));
    } else {
      this.subscribeToSaveResponse(this.livreurService.create(livreur));
    }
  }

  trackCoperativeById(_index: number, item: ICoperative): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ILivreur>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(livreur: ILivreur): void {
    this.editForm.patchValue({
      id: livreur.id,
      idLivreur: livreur.idLivreur,
      idCoperative: livreur.idCoperative,
      nom: livreur.nom,
      prenom: livreur.prenom,
      telephone: livreur.telephone,
      latitude: livreur.latitude,
      longitude: livreur.longitude,
      statutLivreur: livreur.statutLivreur,
      coperative: livreur.coperative,
    });

    this.coperativesSharedCollection = this.coperativeService.addCoperativeToCollectionIfMissing(
      this.coperativesSharedCollection,
      livreur.coperative
    );
  }

  protected loadRelationshipsOptions(): void {
    this.coperativeService
      .query()
      .pipe(map((res: HttpResponse<ICoperative[]>) => res.body ?? []))
      .pipe(
        map((coperatives: ICoperative[]) =>
          this.coperativeService.addCoperativeToCollectionIfMissing(coperatives, this.editForm.get('coperative')!.value)
        )
      )
      .subscribe((coperatives: ICoperative[]) => (this.coperativesSharedCollection = coperatives));
  }

  protected createFromForm(): ILivreur {
    return {
      ...new Livreur(),
      id: this.editForm.get(['id'])!.value,
      idLivreur: this.editForm.get(['idLivreur'])!.value,
      idCoperative: this.editForm.get(['idCoperative'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      latitude: this.editForm.get(['latitude'])!.value,
      longitude: this.editForm.get(['longitude'])!.value,
      statutLivreur: this.editForm.get(['statutLivreur'])!.value,
      coperative: this.editForm.get(['coperative'])!.value,
    };
  }
}
