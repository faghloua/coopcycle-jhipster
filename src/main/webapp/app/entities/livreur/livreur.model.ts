import { ICoperative } from 'app/entities/coperative/coperative.model';
import { ICommande } from 'app/entities/commande/commande.model';
import { StatutLivreur } from 'app/entities/enumerations/statut-livreur.model';

export interface ILivreur {
  id?: number;
  idLivreur?: number;
  idCoperative?: number | null;
  nom?: string;
  prenom?: string;
  telephone?: string;
  latitude?: number;
  longitude?: number;
  statutLivreur?: StatutLivreur;
  coperative?: ICoperative | null;
  commandes?: ICommande[] | null;
}

export class Livreur implements ILivreur {
  constructor(
    public id?: number,
    public idLivreur?: number,
    public idCoperative?: number | null,
    public nom?: string,
    public prenom?: string,
    public telephone?: string,
    public latitude?: number,
    public longitude?: number,
    public statutLivreur?: StatutLivreur,
    public coperative?: ICoperative | null,
    public commandes?: ICommande[] | null
  ) {}
}

export function getLivreurIdentifier(livreur: ILivreur): number | undefined {
  return livreur.id;
}
