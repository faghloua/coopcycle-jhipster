import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ProduitCommandeService } from '../service/produit-commande.service';
import { IProduitCommande, ProduitCommande } from '../produit-commande.model';

import { ProduitCommandeUpdateComponent } from './produit-commande-update.component';

describe('ProduitCommande Management Update Component', () => {
  let comp: ProduitCommandeUpdateComponent;
  let fixture: ComponentFixture<ProduitCommandeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let produitCommandeService: ProduitCommandeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ProduitCommandeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ProduitCommandeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProduitCommandeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    produitCommandeService = TestBed.inject(ProduitCommandeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const produitCommande: IProduitCommande = { id: 456 };

      activatedRoute.data = of({ produitCommande });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(produitCommande));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ProduitCommande>>();
      const produitCommande = { id: 123 };
      jest.spyOn(produitCommandeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ produitCommande });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: produitCommande }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(produitCommandeService.update).toHaveBeenCalledWith(produitCommande);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ProduitCommande>>();
      const produitCommande = new ProduitCommande();
      jest.spyOn(produitCommandeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ produitCommande });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: produitCommande }));
      saveSubject.complete();

      // THEN
      expect(produitCommandeService.create).toHaveBeenCalledWith(produitCommande);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ProduitCommande>>();
      const produitCommande = { id: 123 };
      jest.spyOn(produitCommandeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ produitCommande });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(produitCommandeService.update).toHaveBeenCalledWith(produitCommande);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
