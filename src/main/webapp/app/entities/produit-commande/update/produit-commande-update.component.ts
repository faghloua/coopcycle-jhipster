import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IProduitCommande, ProduitCommande } from '../produit-commande.model';
import { ProduitCommandeService } from '../service/produit-commande.service';

@Component({
  selector: 'jhi-produit-commande-update',
  templateUrl: './produit-commande-update.component.html',
})
export class ProduitCommandeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    idProduit: [null, [Validators.required]],
    idCommande: [null, [Validators.required]],
    quantite: [null, [Validators.required, Validators.max(50)]],
  });

  constructor(
    protected produitCommandeService: ProduitCommandeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ produitCommande }) => {
      this.updateForm(produitCommande);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const produitCommande = this.createFromForm();
    if (produitCommande.id !== undefined) {
      this.subscribeToSaveResponse(this.produitCommandeService.update(produitCommande));
    } else {
      this.subscribeToSaveResponse(this.produitCommandeService.create(produitCommande));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduitCommande>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(produitCommande: IProduitCommande): void {
    this.editForm.patchValue({
      id: produitCommande.id,
      idProduit: produitCommande.idProduit,
      idCommande: produitCommande.idCommande,
      quantite: produitCommande.quantite,
    });
  }

  protected createFromForm(): IProduitCommande {
    return {
      ...new ProduitCommande(),
      id: this.editForm.get(['id'])!.value,
      idProduit: this.editForm.get(['idProduit'])!.value,
      idCommande: this.editForm.get(['idCommande'])!.value,
      quantite: this.editForm.get(['quantite'])!.value,
    };
  }
}
