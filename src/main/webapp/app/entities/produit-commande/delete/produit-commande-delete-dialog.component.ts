import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IProduitCommande } from '../produit-commande.model';
import { ProduitCommandeService } from '../service/produit-commande.service';

@Component({
  templateUrl: './produit-commande-delete-dialog.component.html',
})
export class ProduitCommandeDeleteDialogComponent {
  produitCommande?: IProduitCommande;

  constructor(protected produitCommandeService: ProduitCommandeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.produitCommandeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
