import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProduitCommande } from '../produit-commande.model';

@Component({
  selector: 'jhi-produit-commande-detail',
  templateUrl: './produit-commande-detail.component.html',
})
export class ProduitCommandeDetailComponent implements OnInit {
  produitCommande: IProduitCommande | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ produitCommande }) => {
      this.produitCommande = produitCommande;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
