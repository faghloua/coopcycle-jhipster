import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ProduitCommandeDetailComponent } from './produit-commande-detail.component';

describe('ProduitCommande Management Detail Component', () => {
  let comp: ProduitCommandeDetailComponent;
  let fixture: ComponentFixture<ProduitCommandeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProduitCommandeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ produitCommande: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ProduitCommandeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ProduitCommandeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load produitCommande on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.produitCommande).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
