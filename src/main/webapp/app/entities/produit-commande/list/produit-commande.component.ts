import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProduitCommande } from '../produit-commande.model';
import { ProduitCommandeService } from '../service/produit-commande.service';
import { ProduitCommandeDeleteDialogComponent } from '../delete/produit-commande-delete-dialog.component';

@Component({
  selector: 'jhi-produit-commande',
  templateUrl: './produit-commande.component.html',
})
export class ProduitCommandeComponent implements OnInit {
  produitCommandes?: IProduitCommande[];
  isLoading = false;

  constructor(protected produitCommandeService: ProduitCommandeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.produitCommandeService.query().subscribe({
      next: (res: HttpResponse<IProduitCommande[]>) => {
        this.isLoading = false;
        this.produitCommandes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(_index: number, item: IProduitCommande): number {
    return item.id!;
  }

  delete(produitCommande: IProduitCommande): void {
    const modalRef = this.modalService.open(ProduitCommandeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.produitCommande = produitCommande;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
