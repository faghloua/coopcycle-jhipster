import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ProduitCommandeService } from '../service/produit-commande.service';

import { ProduitCommandeComponent } from './produit-commande.component';

describe('ProduitCommande Management Component', () => {
  let comp: ProduitCommandeComponent;
  let fixture: ComponentFixture<ProduitCommandeComponent>;
  let service: ProduitCommandeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ProduitCommandeComponent],
    })
      .overrideTemplate(ProduitCommandeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProduitCommandeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ProduitCommandeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.produitCommandes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
