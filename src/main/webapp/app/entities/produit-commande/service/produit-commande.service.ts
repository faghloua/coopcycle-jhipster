import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IProduitCommande, getProduitCommandeIdentifier } from '../produit-commande.model';

export type EntityResponseType = HttpResponse<IProduitCommande>;
export type EntityArrayResponseType = HttpResponse<IProduitCommande[]>;

@Injectable({ providedIn: 'root' })
export class ProduitCommandeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/produit-commandes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(produitCommande: IProduitCommande): Observable<EntityResponseType> {
    return this.http.post<IProduitCommande>(this.resourceUrl, produitCommande, { observe: 'response' });
  }

  update(produitCommande: IProduitCommande): Observable<EntityResponseType> {
    return this.http.put<IProduitCommande>(
      `${this.resourceUrl}/${getProduitCommandeIdentifier(produitCommande) as number}`,
      produitCommande,
      { observe: 'response' }
    );
  }

  partialUpdate(produitCommande: IProduitCommande): Observable<EntityResponseType> {
    return this.http.patch<IProduitCommande>(
      `${this.resourceUrl}/${getProduitCommandeIdentifier(produitCommande) as number}`,
      produitCommande,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProduitCommande>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProduitCommande[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addProduitCommandeToCollectionIfMissing(
    produitCommandeCollection: IProduitCommande[],
    ...produitCommandesToCheck: (IProduitCommande | null | undefined)[]
  ): IProduitCommande[] {
    const produitCommandes: IProduitCommande[] = produitCommandesToCheck.filter(isPresent);
    if (produitCommandes.length > 0) {
      const produitCommandeCollectionIdentifiers = produitCommandeCollection.map(
        produitCommandeItem => getProduitCommandeIdentifier(produitCommandeItem)!
      );
      const produitCommandesToAdd = produitCommandes.filter(produitCommandeItem => {
        const produitCommandeIdentifier = getProduitCommandeIdentifier(produitCommandeItem);
        if (produitCommandeIdentifier == null || produitCommandeCollectionIdentifiers.includes(produitCommandeIdentifier)) {
          return false;
        }
        produitCommandeCollectionIdentifiers.push(produitCommandeIdentifier);
        return true;
      });
      return [...produitCommandesToAdd, ...produitCommandeCollection];
    }
    return produitCommandeCollection;
  }
}
