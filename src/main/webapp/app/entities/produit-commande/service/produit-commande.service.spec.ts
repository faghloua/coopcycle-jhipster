import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IProduitCommande, ProduitCommande } from '../produit-commande.model';

import { ProduitCommandeService } from './produit-commande.service';

describe('ProduitCommande Service', () => {
  let service: ProduitCommandeService;
  let httpMock: HttpTestingController;
  let elemDefault: IProduitCommande;
  let expectedResult: IProduitCommande | IProduitCommande[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ProduitCommandeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      idProduit: 0,
      idCommande: 0,
      quantite: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ProduitCommande', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ProduitCommande()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ProduitCommande', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          idProduit: 1,
          idCommande: 1,
          quantite: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ProduitCommande', () => {
      const patchObject = Object.assign(
        {
          idProduit: 1,
          idCommande: 1,
          quantite: 1,
        },
        new ProduitCommande()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ProduitCommande', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          idProduit: 1,
          idCommande: 1,
          quantite: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ProduitCommande', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addProduitCommandeToCollectionIfMissing', () => {
      it('should add a ProduitCommande to an empty array', () => {
        const produitCommande: IProduitCommande = { id: 123 };
        expectedResult = service.addProduitCommandeToCollectionIfMissing([], produitCommande);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(produitCommande);
      });

      it('should not add a ProduitCommande to an array that contains it', () => {
        const produitCommande: IProduitCommande = { id: 123 };
        const produitCommandeCollection: IProduitCommande[] = [
          {
            ...produitCommande,
          },
          { id: 456 },
        ];
        expectedResult = service.addProduitCommandeToCollectionIfMissing(produitCommandeCollection, produitCommande);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ProduitCommande to an array that doesn't contain it", () => {
        const produitCommande: IProduitCommande = { id: 123 };
        const produitCommandeCollection: IProduitCommande[] = [{ id: 456 }];
        expectedResult = service.addProduitCommandeToCollectionIfMissing(produitCommandeCollection, produitCommande);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(produitCommande);
      });

      it('should add only unique ProduitCommande to an array', () => {
        const produitCommandeArray: IProduitCommande[] = [{ id: 123 }, { id: 456 }, { id: 95398 }];
        const produitCommandeCollection: IProduitCommande[] = [{ id: 123 }];
        expectedResult = service.addProduitCommandeToCollectionIfMissing(produitCommandeCollection, ...produitCommandeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const produitCommande: IProduitCommande = { id: 123 };
        const produitCommande2: IProduitCommande = { id: 456 };
        expectedResult = service.addProduitCommandeToCollectionIfMissing([], produitCommande, produitCommande2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(produitCommande);
        expect(expectedResult).toContain(produitCommande2);
      });

      it('should accept null and undefined values', () => {
        const produitCommande: IProduitCommande = { id: 123 };
        expectedResult = service.addProduitCommandeToCollectionIfMissing([], null, produitCommande, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(produitCommande);
      });

      it('should return initial array if no ProduitCommande is added', () => {
        const produitCommandeCollection: IProduitCommande[] = [{ id: 123 }];
        expectedResult = service.addProduitCommandeToCollectionIfMissing(produitCommandeCollection, undefined, null);
        expect(expectedResult).toEqual(produitCommandeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
