import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ProduitCommandeComponent } from '../list/produit-commande.component';
import { ProduitCommandeDetailComponent } from '../detail/produit-commande-detail.component';
import { ProduitCommandeUpdateComponent } from '../update/produit-commande-update.component';
import { ProduitCommandeRoutingResolveService } from './produit-commande-routing-resolve.service';

const produitCommandeRoute: Routes = [
  {
    path: '',
    component: ProduitCommandeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProduitCommandeDetailComponent,
    resolve: {
      produitCommande: ProduitCommandeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProduitCommandeUpdateComponent,
    resolve: {
      produitCommande: ProduitCommandeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProduitCommandeUpdateComponent,
    resolve: {
      produitCommande: ProduitCommandeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(produitCommandeRoute)],
  exports: [RouterModule],
})
export class ProduitCommandeRoutingModule {}
