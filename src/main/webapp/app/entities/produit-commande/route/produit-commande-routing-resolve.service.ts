import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IProduitCommande, ProduitCommande } from '../produit-commande.model';
import { ProduitCommandeService } from '../service/produit-commande.service';

@Injectable({ providedIn: 'root' })
export class ProduitCommandeRoutingResolveService implements Resolve<IProduitCommande> {
  constructor(protected service: ProduitCommandeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProduitCommande> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((produitCommande: HttpResponse<ProduitCommande>) => {
          if (produitCommande.body) {
            return of(produitCommande.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProduitCommande());
  }
}
