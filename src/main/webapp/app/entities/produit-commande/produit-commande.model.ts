import { IProduit } from 'app/entities/produit/produit.model';
import { ICommande } from 'app/entities/commande/commande.model';

export interface IProduitCommande {
  id?: number;
  idProduit?: number;
  idCommande?: number;
  quantite?: number;
  produit?: IProduit | null;
  commandes?: ICommande[] | null;
}

export class ProduitCommande implements IProduitCommande {
  constructor(
    public id?: number,
    public idProduit?: number,
    public idCommande?: number,
    public quantite?: number,
    public produit?: IProduit | null,
    public commandes?: ICommande[] | null
  ) {}
}

export function getProduitCommandeIdentifier(produitCommande: IProduitCommande): number | undefined {
  return produitCommande.id;
}
