import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ProduitCommandeComponent } from './list/produit-commande.component';
import { ProduitCommandeDetailComponent } from './detail/produit-commande-detail.component';
import { ProduitCommandeUpdateComponent } from './update/produit-commande-update.component';
import { ProduitCommandeDeleteDialogComponent } from './delete/produit-commande-delete-dialog.component';
import { ProduitCommandeRoutingModule } from './route/produit-commande-routing.module';

@NgModule({
  imports: [SharedModule, ProduitCommandeRoutingModule],
  declarations: [
    ProduitCommandeComponent,
    ProduitCommandeDetailComponent,
    ProduitCommandeUpdateComponent,
    ProduitCommandeDeleteDialogComponent,
  ],
  entryComponents: [ProduitCommandeDeleteDialogComponent],
})
export class ProduitCommandeModule {}
