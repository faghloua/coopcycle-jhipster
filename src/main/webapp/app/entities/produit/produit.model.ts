import { IProduitCommande } from 'app/entities/produit-commande/produit-commande.model';
import { ICommercant } from 'app/entities/commercant/commercant.model';

export interface IProduit {
  id?: number;
  idProduit?: number;
  nomProduit?: string;
  prix?: number;
  description?: string | null;
  dispinobilite?: number | null;
  produitCommande?: IProduitCommande | null;
  commercant?: ICommercant | null;
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public idProduit?: number,
    public nomProduit?: string,
    public prix?: number,
    public description?: string | null,
    public dispinobilite?: number | null,
    public produitCommande?: IProduitCommande | null,
    public commercant?: ICommercant | null
  ) {}
}

export function getProduitIdentifier(produit: IProduit): number | undefined {
  return produit.id;
}
