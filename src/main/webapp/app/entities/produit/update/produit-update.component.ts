import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IProduit, Produit } from '../produit.model';
import { ProduitService } from '../service/produit.service';
import { IProduitCommande } from 'app/entities/produit-commande/produit-commande.model';
import { ProduitCommandeService } from 'app/entities/produit-commande/service/produit-commande.service';
import { ICommercant } from 'app/entities/commercant/commercant.model';
import { CommercantService } from 'app/entities/commercant/service/commercant.service';

@Component({
  selector: 'jhi-produit-update',
  templateUrl: './produit-update.component.html',
})
export class ProduitUpdateComponent implements OnInit {
  isSaving = false;

  produitCommandesCollection: IProduitCommande[] = [];
  commercantsSharedCollection: ICommercant[] = [];

  editForm = this.fb.group({
    id: [],
    idProduit: [null, [Validators.required]],
    nomProduit: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
    prix: [null, [Validators.required]],
    description: [],
    dispinobilite: [],
    produitCommande: [],
    commercant: [],
  });

  constructor(
    protected produitService: ProduitService,
    protected produitCommandeService: ProduitCommandeService,
    protected commercantService: CommercantService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ produit }) => {
      this.updateForm(produit);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const produit = this.createFromForm();
    if (produit.id !== undefined) {
      this.subscribeToSaveResponse(this.produitService.update(produit));
    } else {
      this.subscribeToSaveResponse(this.produitService.create(produit));
    }
  }

  trackProduitCommandeById(_index: number, item: IProduitCommande): number {
    return item.id!;
  }

  trackCommercantById(_index: number, item: ICommercant): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduit>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(produit: IProduit): void {
    this.editForm.patchValue({
      id: produit.id,
      idProduit: produit.idProduit,
      nomProduit: produit.nomProduit,
      prix: produit.prix,
      description: produit.description,
      dispinobilite: produit.dispinobilite,
      produitCommande: produit.produitCommande,
      commercant: produit.commercant,
    });

    this.produitCommandesCollection = this.produitCommandeService.addProduitCommandeToCollectionIfMissing(
      this.produitCommandesCollection,
      produit.produitCommande
    );
    this.commercantsSharedCollection = this.commercantService.addCommercantToCollectionIfMissing(
      this.commercantsSharedCollection,
      produit.commercant
    );
  }

  protected loadRelationshipsOptions(): void {
    this.produitCommandeService
      .query({ filter: 'produit-is-null' })
      .pipe(map((res: HttpResponse<IProduitCommande[]>) => res.body ?? []))
      .pipe(
        map((produitCommandes: IProduitCommande[]) =>
          this.produitCommandeService.addProduitCommandeToCollectionIfMissing(produitCommandes, this.editForm.get('produitCommande')!.value)
        )
      )
      .subscribe((produitCommandes: IProduitCommande[]) => (this.produitCommandesCollection = produitCommandes));

    this.commercantService
      .query()
      .pipe(map((res: HttpResponse<ICommercant[]>) => res.body ?? []))
      .pipe(
        map((commercants: ICommercant[]) =>
          this.commercantService.addCommercantToCollectionIfMissing(commercants, this.editForm.get('commercant')!.value)
        )
      )
      .subscribe((commercants: ICommercant[]) => (this.commercantsSharedCollection = commercants));
  }

  protected createFromForm(): IProduit {
    return {
      ...new Produit(),
      id: this.editForm.get(['id'])!.value,
      idProduit: this.editForm.get(['idProduit'])!.value,
      nomProduit: this.editForm.get(['nomProduit'])!.value,
      prix: this.editForm.get(['prix'])!.value,
      description: this.editForm.get(['description'])!.value,
      dispinobilite: this.editForm.get(['dispinobilite'])!.value,
      produitCommande: this.editForm.get(['produitCommande'])!.value,
      commercant: this.editForm.get(['commercant'])!.value,
    };
  }
}
