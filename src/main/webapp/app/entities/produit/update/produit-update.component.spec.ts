import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ProduitService } from '../service/produit.service';
import { IProduit, Produit } from '../produit.model';
import { IProduitCommande } from 'app/entities/produit-commande/produit-commande.model';
import { ProduitCommandeService } from 'app/entities/produit-commande/service/produit-commande.service';
import { ICommercant } from 'app/entities/commercant/commercant.model';
import { CommercantService } from 'app/entities/commercant/service/commercant.service';

import { ProduitUpdateComponent } from './produit-update.component';

describe('Produit Management Update Component', () => {
  let comp: ProduitUpdateComponent;
  let fixture: ComponentFixture<ProduitUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let produitService: ProduitService;
  let produitCommandeService: ProduitCommandeService;
  let commercantService: CommercantService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ProduitUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ProduitUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProduitUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    produitService = TestBed.inject(ProduitService);
    produitCommandeService = TestBed.inject(ProduitCommandeService);
    commercantService = TestBed.inject(CommercantService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call produitCommande query and add missing value', () => {
      const produit: IProduit = { id: 456 };
      const produitCommande: IProduitCommande = { id: 74425 };
      produit.produitCommande = produitCommande;

      const produitCommandeCollection: IProduitCommande[] = [{ id: 12484 }];
      jest.spyOn(produitCommandeService, 'query').mockReturnValue(of(new HttpResponse({ body: produitCommandeCollection })));
      const expectedCollection: IProduitCommande[] = [produitCommande, ...produitCommandeCollection];
      jest.spyOn(produitCommandeService, 'addProduitCommandeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ produit });
      comp.ngOnInit();

      expect(produitCommandeService.query).toHaveBeenCalled();
      expect(produitCommandeService.addProduitCommandeToCollectionIfMissing).toHaveBeenCalledWith(
        produitCommandeCollection,
        produitCommande
      );
      expect(comp.produitCommandesCollection).toEqual(expectedCollection);
    });

    it('Should call Commercant query and add missing value', () => {
      const produit: IProduit = { id: 456 };
      const commercant: ICommercant = { id: 32298 };
      produit.commercant = commercant;

      const commercantCollection: ICommercant[] = [{ id: 56048 }];
      jest.spyOn(commercantService, 'query').mockReturnValue(of(new HttpResponse({ body: commercantCollection })));
      const additionalCommercants = [commercant];
      const expectedCollection: ICommercant[] = [...additionalCommercants, ...commercantCollection];
      jest.spyOn(commercantService, 'addCommercantToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ produit });
      comp.ngOnInit();

      expect(commercantService.query).toHaveBeenCalled();
      expect(commercantService.addCommercantToCollectionIfMissing).toHaveBeenCalledWith(commercantCollection, ...additionalCommercants);
      expect(comp.commercantsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const produit: IProduit = { id: 456 };
      const produitCommande: IProduitCommande = { id: 92176 };
      produit.produitCommande = produitCommande;
      const commercant: ICommercant = { id: 85125 };
      produit.commercant = commercant;

      activatedRoute.data = of({ produit });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(produit));
      expect(comp.produitCommandesCollection).toContain(produitCommande);
      expect(comp.commercantsSharedCollection).toContain(commercant);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Produit>>();
      const produit = { id: 123 };
      jest.spyOn(produitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ produit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: produit }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(produitService.update).toHaveBeenCalledWith(produit);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Produit>>();
      const produit = new Produit();
      jest.spyOn(produitService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ produit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: produit }));
      saveSubject.complete();

      // THEN
      expect(produitService.create).toHaveBeenCalledWith(produit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Produit>>();
      const produit = { id: 123 };
      jest.spyOn(produitService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ produit });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(produitService.update).toHaveBeenCalledWith(produit);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackProduitCommandeById', () => {
      it('Should return tracked ProduitCommande primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackProduitCommandeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCommercantById', () => {
      it('Should return tracked Commercant primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCommercantById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
