import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Coperative e2e test', () => {
  const coperativePageUrl = '/coperative';
  const coperativePageUrlPattern = new RegExp('/coperative(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const coperativeSample = { idCoperative: 85247, zoneGeographique: 'supply-chains mint Intelligent' };

  let coperative: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/coperatives+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/coperatives').as('postEntityRequest');
    cy.intercept('DELETE', '/api/coperatives/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (coperative) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/coperatives/${coperative.id}`,
      }).then(() => {
        coperative = undefined;
      });
    }
  });

  it('Coperatives menu should load Coperatives page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('coperative');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Coperative').should('exist');
    cy.url().should('match', coperativePageUrlPattern);
  });

  describe('Coperative page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(coperativePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Coperative page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/coperative/new$'));
        cy.getEntityCreateUpdateHeading('Coperative');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', coperativePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/coperatives',
          body: coperativeSample,
        }).then(({ body }) => {
          coperative = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/coperatives+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [coperative],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(coperativePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Coperative page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('coperative');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', coperativePageUrlPattern);
      });

      it('edit button click should load edit Coperative page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Coperative');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', coperativePageUrlPattern);
      });

      it('last delete button click should delete instance of Coperative', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('coperative').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', coperativePageUrlPattern);

        coperative = undefined;
      });
    });
  });

  describe('new Coperative page', () => {
    beforeEach(() => {
      cy.visit(`${coperativePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Coperative');
    });

    it('should create an instance of Coperative', () => {
      cy.get(`[data-cy="idCoperative"]`).type('20872').should('have.value', '20872');

      cy.get(`[data-cy="zoneGeographique"]`).type('Lorraine Pizza').should('have.value', 'Lorraine Pizza');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        coperative = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', coperativePageUrlPattern);
    });
  });
});
