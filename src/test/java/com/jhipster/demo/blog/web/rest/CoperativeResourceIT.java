package com.jhipster.demo.blog.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.jhipster.demo.blog.IntegrationTest;
import com.jhipster.demo.blog.domain.Coperative;
import com.jhipster.demo.blog.repository.CoperativeRepository;
import com.jhipster.demo.blog.service.dto.CoperativeDTO;
import com.jhipster.demo.blog.service.mapper.CoperativeMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CoperativeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CoperativeResourceIT {

    private static final Integer DEFAULT_ID_COPERATIVE = 1;
    private static final Integer UPDATED_ID_COPERATIVE = 2;

    private static final String DEFAULT_ZONE_GEOGRAPHIQUE = "AAAAAAAAAA";
    private static final String UPDATED_ZONE_GEOGRAPHIQUE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/coperatives";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CoperativeRepository coperativeRepository;

    @Autowired
    private CoperativeMapper coperativeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoperativeMockMvc;

    private Coperative coperative;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coperative createEntity(EntityManager em) {
        Coperative coperative = new Coperative().idCoperative(DEFAULT_ID_COPERATIVE).zoneGeographique(DEFAULT_ZONE_GEOGRAPHIQUE);
        return coperative;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coperative createUpdatedEntity(EntityManager em) {
        Coperative coperative = new Coperative().idCoperative(UPDATED_ID_COPERATIVE).zoneGeographique(UPDATED_ZONE_GEOGRAPHIQUE);
        return coperative;
    }

    @BeforeEach
    public void initTest() {
        coperative = createEntity(em);
    }

    @Test
    @Transactional
    void createCoperative() throws Exception {
        int databaseSizeBeforeCreate = coperativeRepository.findAll().size();
        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);
        restCoperativeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coperativeDTO)))
            .andExpect(status().isCreated());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeCreate + 1);
        Coperative testCoperative = coperativeList.get(coperativeList.size() - 1);
        assertThat(testCoperative.getIdCoperative()).isEqualTo(DEFAULT_ID_COPERATIVE);
        assertThat(testCoperative.getZoneGeographique()).isEqualTo(DEFAULT_ZONE_GEOGRAPHIQUE);
    }

    @Test
    @Transactional
    void createCoperativeWithExistingId() throws Exception {
        // Create the Coperative with an existing ID
        coperative.setId(1L);
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        int databaseSizeBeforeCreate = coperativeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoperativeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coperativeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkIdCoperativeIsRequired() throws Exception {
        int databaseSizeBeforeTest = coperativeRepository.findAll().size();
        // set the field null
        coperative.setIdCoperative(null);

        // Create the Coperative, which fails.
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        restCoperativeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coperativeDTO)))
            .andExpect(status().isBadRequest());

        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkZoneGeographiqueIsRequired() throws Exception {
        int databaseSizeBeforeTest = coperativeRepository.findAll().size();
        // set the field null
        coperative.setZoneGeographique(null);

        // Create the Coperative, which fails.
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        restCoperativeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coperativeDTO)))
            .andExpect(status().isBadRequest());

        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCoperatives() throws Exception {
        // Initialize the database
        coperativeRepository.saveAndFlush(coperative);

        // Get all the coperativeList
        restCoperativeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coperative.getId().intValue())))
            .andExpect(jsonPath("$.[*].idCoperative").value(hasItem(DEFAULT_ID_COPERATIVE)))
            .andExpect(jsonPath("$.[*].zoneGeographique").value(hasItem(DEFAULT_ZONE_GEOGRAPHIQUE)));
    }

    @Test
    @Transactional
    void getCoperative() throws Exception {
        // Initialize the database
        coperativeRepository.saveAndFlush(coperative);

        // Get the coperative
        restCoperativeMockMvc
            .perform(get(ENTITY_API_URL_ID, coperative.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coperative.getId().intValue()))
            .andExpect(jsonPath("$.idCoperative").value(DEFAULT_ID_COPERATIVE))
            .andExpect(jsonPath("$.zoneGeographique").value(DEFAULT_ZONE_GEOGRAPHIQUE));
    }

    @Test
    @Transactional
    void getNonExistingCoperative() throws Exception {
        // Get the coperative
        restCoperativeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCoperative() throws Exception {
        // Initialize the database
        coperativeRepository.saveAndFlush(coperative);

        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();

        // Update the coperative
        Coperative updatedCoperative = coperativeRepository.findById(coperative.getId()).get();
        // Disconnect from session so that the updates on updatedCoperative are not directly saved in db
        em.detach(updatedCoperative);
        updatedCoperative.idCoperative(UPDATED_ID_COPERATIVE).zoneGeographique(UPDATED_ZONE_GEOGRAPHIQUE);
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(updatedCoperative);

        restCoperativeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coperativeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coperativeDTO))
            )
            .andExpect(status().isOk());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
        Coperative testCoperative = coperativeList.get(coperativeList.size() - 1);
        assertThat(testCoperative.getIdCoperative()).isEqualTo(UPDATED_ID_COPERATIVE);
        assertThat(testCoperative.getZoneGeographique()).isEqualTo(UPDATED_ZONE_GEOGRAPHIQUE);
    }

    @Test
    @Transactional
    void putNonExistingCoperative() throws Exception {
        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();
        coperative.setId(count.incrementAndGet());

        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoperativeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coperativeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coperativeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCoperative() throws Exception {
        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();
        coperative.setId(count.incrementAndGet());

        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoperativeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coperativeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCoperative() throws Exception {
        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();
        coperative.setId(count.incrementAndGet());

        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoperativeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coperativeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCoperativeWithPatch() throws Exception {
        // Initialize the database
        coperativeRepository.saveAndFlush(coperative);

        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();

        // Update the coperative using partial update
        Coperative partialUpdatedCoperative = new Coperative();
        partialUpdatedCoperative.setId(coperative.getId());

        partialUpdatedCoperative.idCoperative(UPDATED_ID_COPERATIVE);

        restCoperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoperative.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoperative))
            )
            .andExpect(status().isOk());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
        Coperative testCoperative = coperativeList.get(coperativeList.size() - 1);
        assertThat(testCoperative.getIdCoperative()).isEqualTo(UPDATED_ID_COPERATIVE);
        assertThat(testCoperative.getZoneGeographique()).isEqualTo(DEFAULT_ZONE_GEOGRAPHIQUE);
    }

    @Test
    @Transactional
    void fullUpdateCoperativeWithPatch() throws Exception {
        // Initialize the database
        coperativeRepository.saveAndFlush(coperative);

        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();

        // Update the coperative using partial update
        Coperative partialUpdatedCoperative = new Coperative();
        partialUpdatedCoperative.setId(coperative.getId());

        partialUpdatedCoperative.idCoperative(UPDATED_ID_COPERATIVE).zoneGeographique(UPDATED_ZONE_GEOGRAPHIQUE);

        restCoperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoperative.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoperative))
            )
            .andExpect(status().isOk());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
        Coperative testCoperative = coperativeList.get(coperativeList.size() - 1);
        assertThat(testCoperative.getIdCoperative()).isEqualTo(UPDATED_ID_COPERATIVE);
        assertThat(testCoperative.getZoneGeographique()).isEqualTo(UPDATED_ZONE_GEOGRAPHIQUE);
    }

    @Test
    @Transactional
    void patchNonExistingCoperative() throws Exception {
        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();
        coperative.setId(count.incrementAndGet());

        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, coperativeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coperativeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCoperative() throws Exception {
        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();
        coperative.setId(count.incrementAndGet());

        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoperativeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coperativeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCoperative() throws Exception {
        int databaseSizeBeforeUpdate = coperativeRepository.findAll().size();
        coperative.setId(count.incrementAndGet());

        // Create the Coperative
        CoperativeDTO coperativeDTO = coperativeMapper.toDto(coperative);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoperativeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(coperativeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coperative in the database
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCoperative() throws Exception {
        // Initialize the database
        coperativeRepository.saveAndFlush(coperative);

        int databaseSizeBeforeDelete = coperativeRepository.findAll().size();

        // Delete the coperative
        restCoperativeMockMvc
            .perform(delete(ENTITY_API_URL_ID, coperative.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Coperative> coperativeList = coperativeRepository.findAll();
        assertThat(coperativeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
