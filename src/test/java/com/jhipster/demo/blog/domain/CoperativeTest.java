package com.jhipster.demo.blog.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.jhipster.demo.blog.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoperativeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Coperative.class);
        Coperative coperative1 = new Coperative();
        coperative1.setId(1L);
        Coperative coperative2 = new Coperative();
        coperative2.setId(coperative1.getId());
        assertThat(coperative1).isEqualTo(coperative2);
        coperative2.setId(2L);
        assertThat(coperative1).isNotEqualTo(coperative2);
        coperative1.setId(null);
        assertThat(coperative1).isNotEqualTo(coperative2);
    }
}
