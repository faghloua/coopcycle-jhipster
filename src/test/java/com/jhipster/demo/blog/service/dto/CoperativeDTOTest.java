package com.jhipster.demo.blog.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.jhipster.demo.blog.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoperativeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CoperativeDTO.class);
        CoperativeDTO coperativeDTO1 = new CoperativeDTO();
        coperativeDTO1.setId(1L);
        CoperativeDTO coperativeDTO2 = new CoperativeDTO();
        assertThat(coperativeDTO1).isNotEqualTo(coperativeDTO2);
        coperativeDTO2.setId(coperativeDTO1.getId());
        assertThat(coperativeDTO1).isEqualTo(coperativeDTO2);
        coperativeDTO2.setId(2L);
        assertThat(coperativeDTO1).isNotEqualTo(coperativeDTO2);
        coperativeDTO1.setId(null);
        assertThat(coperativeDTO1).isNotEqualTo(coperativeDTO2);
    }
}
